/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.templ.ThymeleafTemplateEngine;
import io.wx.core3.app.beans.User;
import io.wx.core3.common.WXTools;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.SimpleConfig;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
@RunWith(VertxUnitRunner.class)
public class TraitTest {
  public static MyApplication myApp;
  public static String RESOURCE_URL = "/hello-traits";

  @BeforeClass
  public static void setUpApplication() throws Exception {
    AppConfig cfg = new SimpleConfig().setPort(1337).setInstanceCount(1)
                                          .setStage(AppConfig.Stage.LOCAL)
                                          .setTemplateEngine(ThymeleafTemplateEngine.create());
    cfg.getTemplateRoutes().add(".+\\.html"); 
    
    myApp = new MyApplication(cfg);
    myApp.start();
    Thread.sleep(1000);
  }
  
  @AfterClass
  public static void setDownApplication() throws Exception {
    myApp.getCore().getVertx().close();
    myApp = null;
  }
  
  @Test
  public void testTraits(TestContext context) {
    String expected = "io.wx.core3.app.controller.HelloWorldWithTraits->helloworld1: [In MyOtherTrait Handler]";
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/world");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        context.assertEquals(expected, body.toString());
        async1.complete();
      });
    });
    req.end();
  }
  
  @Test
  public void testOrderTraits(TestContext context) {
    String expected = "io.wx.core3.app.controller.HelloWorldWithTraits->helloworld2: [In MyOtherTrait Handler]";
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/world2");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        context.assertEquals(expected, body.toString());
        async1.complete();
      });
    });
    req.end();
  }
  
  @Test
  public void testTraitsWithPOST(TestContext context) {
    String expected = "[In MyOtherTrait Handler]-----";
    User user = new User();
    user.setName(expected);
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.post(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/world");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        context.assertEquals(expected + WXTools.toJSON(user), body.toString());
        async1.complete();
      });
    });
    req.putHeader("content-type", "application/json");
    req.putHeader("content-length", "" + WXTools.toJSON(user).length());
    req.write(WXTools.toJSON(user));
    req.end();
  }
  
  @Test
  public void testReadBodywithoutBodyTrait(TestContext context) {
    String expected = "[In MyOtherTrait Handler]-----null";
    User user = new User();
    user.setName(expected);
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.put(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/world");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        context.assertEquals(expected , body.toString());
        async1.complete();
      });
    });
    req.putHeader("content-type", "application/json");
    req.putHeader("content-length", "" + WXTools.toJSON(user).length());
    req.write(WXTools.toJSON(user));
    req.end();
  }
}
