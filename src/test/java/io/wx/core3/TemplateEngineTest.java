/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.templ.ThymeleafTemplateEngine;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.SimpleConfig;
import io.wx.core3.http.HttpStatus;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Jose Luis Conde Linares - jlinares@aptly.de
 */
@RunWith(VertxUnitRunner.class)
public class TemplateEngineTest {

    public static MyApplication myApp;
    public static String RESOURCE_URL = "/template-engine";

    @BeforeClass
    public static void setUpApplication() throws Exception {
        AppConfig cfg = new SimpleConfig().setPort(1337).setInstanceCount(1)
                                          .setStage(AppConfig.Stage.LOCAL)
                                          .setTemplateEngine(ThymeleafTemplateEngine.create());
        cfg.getTemplateRoutes().add(".+\\.html");

        myApp = new MyApplication(cfg);
        myApp.start();
        Thread.sleep(1000);
    }

    @AfterClass
    public static void setDownApplication() throws Exception {
        myApp.getCore().getVertx().close();
        myApp = null;
    }

    @Test
    public void testTemplateRendern(TestContext context) {
        final String expected = "john";
        Async async1 = context.async();
        HttpClient client = myApp.getCore().getVertx().createHttpClient();
        HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/user?username=" + expected);
        req.exceptionHandler(err -> context.fail(err.getMessage()));
        req.handler(resp -> {
            context.assertEquals(200, resp.statusCode());
            resp.bodyHandler(body -> {
                context.assertTrue(body.toString().contains(expected));
                async1.complete();
            });
        });
        req.end();
    }

    @Test
    public void testTemplateAsynRendern(TestContext context) {
        final String expected = "johnAsyn";
        Async async1 = context.async();
        HttpClient client = myApp.getCore().getVertx().createHttpClient();
        HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/user/asyn?username=" + expected);
        req.exceptionHandler(err -> context.fail(err.getMessage()));
        req.handler(resp -> {
            context.assertEquals(HttpStatus.GATEWAY_TIMEOUT.value(), resp.statusCode());
            resp.bodyHandler(body -> {
                context.assertTrue(body.toString().contains(expected));
                async1.complete();
            });
        });
        req.end();
    }

}
