/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.templ.ThymeleafTemplateEngine;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.SimpleConfig;
import java.net.HttpCookie;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
@RunWith(VertxUnitRunner.class)
public class StatefulFieldTest {

  public static MyApplication myApp;
  public static String RESOURCE_URL = "/testfields";

  @BeforeClass
  public static void setUpApplication() throws Exception {
    AppConfig cfg = new SimpleConfig().setPort(1337).setInstanceCount(1)
                                          .setStage(AppConfig.Stage.LOCAL)
                                          .setTemplateEngine(ThymeleafTemplateEngine.create());
    cfg.getTemplateRoutes().add(".+\\.html"); 
    
    myApp = new MyApplication(cfg);
    myApp.start();
    Thread.sleep(1000);
  }

  @AfterClass
  public static void setDownApplication() throws Exception {
    myApp.getCore().getVertx().close();
    myApp = null;
  }
  
  @Test
  public void testOrder(TestContext context) {
    String expected1 = "setting name to Johnhi";
    String expected2 = "hi John";
    String expectedAnotherClient = "hi null";
    StringBuffer cookies = new StringBuffer();
    Async async1 = context.async();
    
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/set?name=John");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      Integer numCookies = resp.cookies().size();
      for(int i = 0; i < numCookies ; i++){
        for(HttpCookie cookie : HttpCookie.parse(resp.cookies().get(i))){
          cookies.append(cookie.getName()).append("=").append(cookie.getValue()).append(";");
        }
      }
      resp.bodyHandler(body -> {
        context.assertEquals(expected1, body.toString());
        async1.complete();
      });
    });
    req.end();
    
    async1.awaitSuccess();
    
    Async async2 = context.async();
    req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL);
    req.handler(response -> {
      context.assertEquals(200, response.statusCode());
      response.bodyHandler(body -> {
        context.assertEquals(expected2, body.toString());
        async2.complete();
      });
    });
    req.putHeader("Cookie", cookies);
    req.end();

    Async async3 = context.async();
    req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL);
    req.handler(response -> {
      context.assertEquals(200, response.statusCode());
      response.bodyHandler(body2 -> {
        context.assertEquals(expectedAnotherClient, body2.toString());
        async3.complete();
      });
    });
    req.end();
  }
}
