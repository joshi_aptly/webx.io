/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3;

import io.wx.core3.app.config.ExampleConfigurator;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import static io.wx.core3.TemplateEngineTest.myApp;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.SimpleConfig;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Jose Luis Conde Linares - jlinares@aptly.de
 */
@RunWith(VertxUnitRunner.class)
public class ConfigurationTest {

    @BeforeClass
    public static void setUpApplication() throws Exception {
        AppConfig cfg = new SimpleConfig().setPort(1337).setInstanceCount(1)
                                          .setStage(AppConfig.Stage.LOCAL);
        myApp = new MyApplication(cfg, new ExampleConfigurator());
        myApp.start();
        Thread.sleep(1000);
    }

    @AfterClass
    public static void setDownApplication() throws Exception {
        myApp.getCore().getVertx().close();
    }

    @Test
    public void testTemplateRendern(TestContext context) {
        //config values set from example configurator
        context.assertEquals(  myApp.getConfig().getAddonCredentials().getString("testkey") , "testvalue" );
        context.assertEquals( myApp.getConfig().getStage(), AppConfig.Stage.TEST);

    }

}
