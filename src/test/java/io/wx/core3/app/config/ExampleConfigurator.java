/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.config;

import io.wx.core3.config.AbstractConfigurator;
import io.wx.core3.config.AppConfig;
import java.util.Map;

/**
 *
 * @author moritz
 */
public class ExampleConfigurator extends AbstractConfigurator{

    @Override
    public void doConfig(Map modules) {
        AppConfig cfg = getAppConfig();
        if(cfg.getStage().equals(AppConfig.Stage.LOCAL)){
            cfg.setStage(AppConfig.Stage.TEST);
        }
        cfg.getAddonCredentials().put("testkey", "testvalue");
    }
    

}
