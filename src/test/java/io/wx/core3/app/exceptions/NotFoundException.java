/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.exceptions;

import io.wx.core3.http.exceptions.StatusCode;

/**
 *
 * @author moritz
 */
@StatusCode(value = 404)
public class NotFoundException extends Exception {

    /**
     * Creates a new instance of <code>NotFound</code> without detail message.
     */
    public NotFoundException() {
    }

    /**
     * Constructs an instance of <code>NotFound</code> with the specified detail
     * message.
     *
     * @param msg the detail message.
     */
    public NotFoundException(String msg) {
        super(msg);
    }
}
