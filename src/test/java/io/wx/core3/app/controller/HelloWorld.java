/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.controller;

import io.vertx.core.http.HttpMethod;
import io.wx.core3.app.beans.URLBean;
import io.wx.core3.http.Resource;
import io.wx.core3.http.URLParam;
import io.wx.core3.http.session.Stateful;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.traits.TraitConf;
import io.wx.core3.http.traits.CookieSessionTrait;
import io.wx.core3.app.beans.User;
import io.wx.core3.http.traits.BodyHandlerTrait;
import io.wx.core3.app.traits.MyJSONTrait;
import io.wx.core3.http.HttpStatus;
import io.wx.core3.http.URLParamConsumer;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.function.Consumer;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
@Resource(path = "/hello", traits = { @TraitConf(trait = CookieSessionTrait.class) })
public class HelloWorld {
    private static Logger logger = LogManager.getLogger(HelloWorld.class);    
    @Inject RequestController ctr;
    @Stateful public Integer count=0;
    @Stateful public String testvalue = "default";
    @Stateful public User user;
    
    
    
    @RequestMapping(path = "/and")
    public String testAnd(){
        ctr.getRequest().params().forEach(new Consumer<Map.Entry<String, String>>() {
            @Override
            public void accept(Map.Entry<String, String> t) {
                logger.info(t.getKey()+"=>"+t.getValue());
            }
        });
        return "hi";
    }
    
    @RequestMapping(path = "/httpStatus", httpStatus = HttpStatus.ACCEPTED)
    public String testResponseStatus(){
        return "request accepted";
    }
    
    @RequestMapping(path = "/urlbean")
    public void helloURLBean(@URLParamConsumer URLBean bean){   
        
        ctr.next();
    }
    
    
    /***
     * if we define a specific order for a handler, we need to make sure that ALL matching handlers are having 
     * a unique order. by default vertx is setting the order of the first handler to 1 , the order of the second handler to 2....
     * 
     * @return 
     */
    @RequestMapping(path = "/world", order = 101)
    public String helloworld(){
        count++;
        return " HelloWorld->hello world 1 --> "+count;
    }
        
    @RequestMapping(path = "/world", order = 100)
    public String helloworld2(){
        count++;
        return "HelloWorld->hello world 2 --> "+count;
    }
    
    @RequestMapping(path = "/world", order = 102)
    public String helloworld3(){
        count++;
        return " HelloWorld->hello world 3 --> "+count;
    }
    
    /**
     * cheap example of aync handling, call next whenever you ready
     */
    @RequestMapping(path = "/void")
    public void helloVoid(){
        ctr.getResponseBody().append("HelloWorld + hello void");
        ctr.next();
    }
    
    @RequestMapping(path = "/error")
    public void testerror(){
        RuntimeException rt = new RuntimeException("oh man");
        ctr.fail(rt);
    }
 
    @RequestMapping(path = "/exception")
    public void testException(){
        throw  new RuntimeException("oh man");
    }
    
    
    @RequestMapping(path = "/user")
    public User helloUser(@URLParam(name = "username") String username){
        if(this.user==null){
            this.user = new User().setName(username);
        }
        else{
            if(username!=null){
                this.user.setName(username);
            }
        }
        return this.user;
    }
    
    @RequestMapping( method = HttpMethod.PUT, path = "/user", traits = { @TraitConf(trait = BodyHandlerTrait.class)})
    public User helloPut(User u){
        this.user = u;
        return u;
    }

        
    @RequestMapping( method = HttpMethod.PUT, path = "/body", traits = @TraitConf(trait = BodyHandlerTrait.class))
    public String helloPut(){
        return "-"+ctr.getRequestBodyAsString();
    }
    

    /****
     * Caused by: java.lang.IllegalStateException: You must set the Content-Length header to be the total size of the message body BEFORE 
 sending any data if you are not using HTTP_METHOD chunked encoding.
     * @return 
     */
    @RequestMapping (path = "/write")
    public String writeCommand() throws UnsupportedEncodingException{
        ctr.getResponse().putHeader("Content-length", String.valueOf("hi write".getBytes("UTF-8").length));
        ctr.getResponse().write("hi write");
        //ctr.next();
        return "hi return";
    }
 
    @RequestMapping (path = "/session")
    public String helloSession(){
        return "hi session "+ctr.getSession()+" vs "+ctr.getRoutingContext().session();
    }
    
    @RequestMapping (path = "/count")
    public String count(){
        count++;
        return count.toString();       
    }
    
    @RequestMapping (path = "/testsession")
    public String helloSessionValue(@URLParam(name = "value") String value){
        if(value != null){
            this.testvalue = value;
            logger.info("setting testvalue to "+value);
        }
        return this.testvalue;
    }

    @RequestMapping(path = "/world", order = 105,traits = @TraitConf(trait = MyJSONTrait.class))
    public String helloworldJSON(){
        count++;
        return " HelloWorld->hello world JSON --> "+count;
    }
    
}
