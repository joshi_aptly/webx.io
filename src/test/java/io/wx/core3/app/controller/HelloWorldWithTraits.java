/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.controller;

import io.vertx.core.http.HttpMethod;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.Resource;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.traits.TraitConf;
import io.wx.core3.http.traits.BodyHandlerTrait;
import io.wx.core3.app.traits.MyLazyTrait;
import io.wx.core3.app.traits.MyOtherTrait;
import io.wx.core3.app.traits.MyTrait;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
@Resource(path = "/hello-traits", traits = {@TraitConf(trait = MyTrait.class), @TraitConf(trait = MyTrait.class)})
public class HelloWorldWithTraits{
    private static Logger logger = LogManager.getLogger(HelloWorldWithTraits.class);    
    @Inject RequestController ctr;
    
    @RequestMapping(path = "/world", traits = {@TraitConf(trait = MyLazyTrait.class), @TraitConf(trait = MyOtherTrait.class)})
    public String helloworld1(){      
      logger.info("###    running Controller Method");
      return this.getClass().getName() + "->helloworld1: " + ctr.getRoutingContext().data().get("Trait");
    }
        
    @RequestMapping(path = "/world2", traits = {@TraitConf(trait = MyOtherTrait.class), @TraitConf(trait = MyTrait.class)})
    public String helloworld2(){
        return this.getClass().getName() + "->helloworld2: " + ctr.getRoutingContext().data().get("Trait");
    }
    
    @RequestMapping( method = HttpMethod.POST, path = "/world", traits = { @TraitConf(trait = MyOtherTrait.class), @TraitConf(trait = BodyHandlerTrait.class)})
    public String helloworldPOST(){
        return ctr.getRoutingContext().data().get("Trait") + "-----" + ctr.getRequestBodyAsString();
    }

    /**
     * This method is unable to read the body because it does not define the BodyHandlerTrait
     * 
     */
    @RequestMapping( method = HttpMethod.PUT, path = "/world", traits = {@TraitConf(trait = MyOtherTrait.class)})
    public String helloworldPUT(){
        return ctr.getRoutingContext().data().get("Trait") + "-----" + ctr.getRequestBodyAsString();
    }
    
    @RequestMapping( consumes = "*/json",    traits = {@TraitConf(trait = MyOtherTrait.class)})
    public String helloworld1WithoutPath(){      
      logger.info("###    running Controller Method without Path");
      return this.getClass().getName() + "->helloworld1WithoutPath: " + ctr.getRoutingContext().data().get("Trait");
    }
    
}
