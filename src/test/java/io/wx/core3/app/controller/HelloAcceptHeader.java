/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.controller;

import io.wx.core3.examples.auth.beans.User;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.Resource;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * Test Content-Type be accept header
 *
 * @author moritz
 */
@Resource(path = "/accept")
public class HelloAcceptHeader {   
    private static Logger logger = LogManager.getLogger(HelloAcceptHeader.class);      
    @Inject RequestController ctr;

    
    @RequestMapping(path = "/everything/user")
    public User getUser(){ //accept everything, so return object as json
        User user = new User();
        user.setUsername("jlconde");
        logger.info("getUser() "+ctr.getAcceptableContentType());
        return user;
    }

    @RequestMapping(path = "/everything/string")    
    public String getString(){
        return "hi";
    }
    
    @RequestMapping(path = "/xml/user", produces = "application/xml")   
    public User getUserAsXml(){
        return getUser();
    }
    
    @RequestMapping(path = "/html", produces = "text/html")
    public String getHtml(){
        logger.info("getHtml() "+ctr.getAcceptableContentType());
        return "<h1>hi</h1>";
    }
    
    @RequestMapping(path = "/pdf" , produces = "application/pdf")
    public String getPDF(){
        return "mypdfdude";
    }
  
}
