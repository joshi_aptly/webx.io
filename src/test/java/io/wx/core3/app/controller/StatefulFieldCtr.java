/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.controller;

import io.vertx.core.http.HttpMethod;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.Resource;
import io.wx.core3.http.URLParam;
import io.wx.core3.http.session.Stateful;
import io.wx.core3.http.traits.CookieSessionTrait;
import io.wx.core3.http.traits.TraitConf;
import javax.inject.Inject;

/**
 *
 * @author moritz
 */
@Resource(path = "/testfields", traits = @TraitConf(trait = CookieSessionTrait.class))
public class StatefulFieldCtr {
    @Stateful public String username;
    @Inject  RequestController ctr;

    
    
    @RequestMapping(method = HttpMethod.GET, path = "/set")
    public String pushUsername(@URLParam(name = "name") String name){
       this.username = name;
       ctr.getResponseBody().append("setting name to ").append(username);
       return "hi";
    }
    
   @RequestMapping(method = HttpMethod.GET)    
    public String returnUsername(){
        return "hi "+username;
    }
    
    
}
