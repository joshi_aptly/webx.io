/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.controller;

import io.wx.core3.app.beans.User;
import io.wx.core3.http.HttpStatus;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.Resource;
import io.wx.core3.http.URLParam;
import io.wx.core3.http.exceptions.HandleResponseException;
import io.wx.core3.http.session.Stateful;
import io.wx.core3.http.traits.CookieSessionTrait;
import io.wx.core3.http.traits.TraitConf;
import io.wx.core3.templates.Template;
import javax.inject.Inject;

/**
 *
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
@Resource(path = "/template-engine", traits = @TraitConf(trait = CookieSessionTrait.class))
public class HelloWorldTemplate {
  @Inject RequestController ctr;
  @Stateful public User user;
    
  @RequestMapping(path = "/user")
  public Template helloUserWithTemplate(@URLParam(name = "username") String username){
      if(this.user==null){
          this.user = new User().setName(username);
      }
      else{
          if(username!=null){
              this.user.setName(username);
          }
      }
      ctr.getRoutingContext().put("user", user);
      return new Template("templates/user");
  }
  
  
  @RequestMapping(path = "/user/asyn", httpStatus = HttpStatus.GATEWAY_TIMEOUT)
  public void helloUserAsynWithTemplate(@URLParam(name = "username") String username){
    try{
      if(this.user==null){
          this.user = new User().setName(username);
      }
      else{
          if(username!=null){
              this.user.setName(username);
          }
      }
      ctr.getRoutingContext().put("user", user);
      ctr.doResponse(new Template("templates/user"));
      ctr.next();
    }catch(HandleResponseException e){
      ctr.fail(e);
    }
  }
}
