/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.controller;

import io.vertx.core.http.HttpMethod;
import io.wx.core3.app.beans.ValidatedBean;
import io.wx.core3.app.exceptions.ComplicatedCause;
import io.wx.core3.app.exceptions.NotFoundException;
import io.wx.core3.app.exceptions.TestException;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.Resource;
import io.wx.core3.http.traits.BodyHandlerTrait;
import io.wx.core3.http.traits.TraitConf;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @todo validation exception , invalid entity exception
 * 
 * @author moritz
 */
@Resource(path = "/exceptions",  traits = {@TraitConf(trait = BodyHandlerTrait.class)})
public class HelloException {
    private static Logger logger = LogManager.getLogger(HelloException.class);       
    @Inject RequestController ctr;
    
    @RequestMapping(path = "/throwable")
    public String helloThrowable() throws Throwable{
        throw new Throwable("hi");
    }
    
    
    @RequestMapping
    public String hello() throws TestException{
        throw new TestException("im have a wierd http code", new ComplicatedCause("its complicated")); //not logged by default since no 500 exception
    }
    
    @RequestMapping(path = "/async")
    public void asyncException(){
        ctr.fail(new NotFoundException("not found, dude"));        
    }
    
    @RequestMapping(path = "/nullpointer")
    public void nullPointer() throws TestException{
       HelloException h = null;
       h.hello();
       ctr.next();
    }
    
    
    @RequestMapping(path = "/validate", method = HttpMethod.POST)
    public void testValidation(ValidatedBean b){
        ctr.next();
    }
    
    
}
