/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.controller;

import io.wx.core3.http.Resource;
import io.wx.core3.http.URLParam;
import io.wx.core3.http.session.Stateful;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.traits.TraitConf;
import io.wx.core3.http.traits.CookieSessionTrait;
import java.util.Map;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
@Stateful
@Resource(path = "/stateful", traits = @TraitConf(trait = CookieSessionTrait.class))
public class HelloStatefulClass {
    private static Logger logger = LogManager.getLogger(HelloStatefulClass.class);  
    public int count=0;
    @Inject RequestController ctr;
    
    @RequestMapping(path = "/count", order = 1)
    public String countUp(){
        logger.info("count up"+count);
        count++;
        return ""+count;
    }

    @RequestMapping(path = "/count", order = 2)
    public String countAgain(){
        logger.info("count again"+count);
        count++;
        return "+1 = "+count;
    }
    
    @RequestMapping(order = 3)
    public String helloStateful(){
        return "hello stateful";
    }
    
    @RequestMapping(order = 4, regex = ".*") 
    public String trace(){
        String result = "<br/><p>trace:";
        for (Map.Entry<String, Object> entrySet : ctr.getData().entrySet()) {
            result +=  entrySet.getKey()+" =>"+ entrySet.getValue()+";";
        }
        return result+"</p>";
    }
    
    @RequestMapping(path = "/session")
    public Map readcount(@URLParam(name = "key") String key,
                         @URLParam(name = "value") String value){
        if(key != null){
            ctr.getSession().put(key, value);
        }
        return ctr.getSession().data();
    }
}
