/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.templ.ThymeleafTemplateEngine;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.SimpleConfig;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
@RunWith(VertxUnitRunner.class)
public class HeaderBasedRoutingTest {
  public static MyApplication myApp;
  public static String RESOURCE_URL = "/hello-consumes";
  public static String RESOURCE_TEMPLATE_URL = "/template-engine";

  @BeforeClass
  public static void setUpApplication() throws Exception {
    AppConfig cfg = new SimpleConfig().setPort(1337).setInstanceCount(1)
                                                    .setStage(AppConfig.Stage.LOCAL)
                                                    .setTemplateEngine(ThymeleafTemplateEngine.create());
    cfg.getTemplateRoutes().add(".+\\.html"); 
    
    myApp = new MyApplication(cfg);
    myApp.start();
    Thread.sleep(1000);
  }
  
  @AfterClass
  public static void setDownApplication() throws Exception {
    myApp.getCore().getVertx().close();
    myApp = null;
  }
  
  @Test
  public void testWrongContentType(TestContext context) {
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/world1");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(404, resp.statusCode());  
      async1.complete();
    });
    req.end();
  }
  
  @Test
  public void testRightContentType(TestContext context) {
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/world1");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());      
      async1.complete();
    });
    req.putHeader("Content-Type", "application/json");
    req.end();
  }
  
  @Test
  public void testWrongAcceptType(TestContext context) {
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/world1");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(404, resp.statusCode());  
      async1.complete();
    });
    req.putHeader("Content-Type", "application/json");
    req.putHeader("Accept", "text/xml");
    req.end();
  }
  
  @Test
  public void testRightAcceptType(TestContext context) {
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_TEMPLATE_URL + "/user?username=john");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());  
      async1.complete();
    });
    req.putHeader("Content-Type", "application/json");
    req.putHeader("Accept", "text/html");
    req.end();
  }
}
