/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3;

import io.vertx.ext.web.templ.ThymeleafTemplateEngine;
import io.wx.core3.app.controller.HelloWorld;
import io.wx.core3.app.controller.HelloWorldTemplate;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.Configurator;
import io.wx.core3.config.SimpleConfig;
import io.wx.core3.http.app.AbstractApplication;
import io.wx.core3.http.routing.StaticRoute;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
public class MyApplication extends AbstractApplication{
    private static Logger logger = LogManager.getLogger(MyApplication.class);
    
    
    public static void main(String[] args) throws Exception {
        AppConfig cfg = new SimpleConfig().setPort(1337).setInstanceCount(1)
                                          .setStage(AppConfig.Stage.LOCAL)
                                          .setTemplateEngine(ThymeleafTemplateEngine.create());
        cfg.getTemplateRoutes().add(".+\\.html");    
        cfg.getPublicRoutes().add(new StaticRoute(".+\\.js", "public", true));
        
        cfg.getModuleController().put("/testmodulecontroller", HelloWorld.class);
        cfg.getModuleController().put("/testmodulecontroller",  HelloWorldTemplate.class);
                
        MyApplication instance = new MyApplication(cfg);
        instance.start().idle();  //@todo implement run() : start + idle?      
    }
    
    
    public MyApplication( AppConfig cfg ) throws Exception{
        super( cfg );        
    }

    public MyApplication( AppConfig cfg, Configurator... configurator ) throws Exception{
        super( cfg, configurator );        
    }    
    @Override
    public String getApplicationPath() {
       return "io.wx.core3.app";
    }
    
}

