/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.examples.auth;

import com.google.inject.AbstractModule;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import io.wx.core3.examples.auth.beans.Token;
import io.wx.core3.examples.auth.beans.User;
import io.wx.core3.common.WXTools;
import io.wx.core3.config.AppConfig;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.app.Application;
import io.wx.core3.http.traits.Trait;
import java.net.URLDecoder;
import java.util.Date;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
public class LoginTrait implements Trait {
    private static final Logger logger = LogManager.getLogger(LoginTrait.class);     
 
    @Override
    public Handler<RoutingContext> setupBeforeTrait(Application a) throws Exception {
        return new io.wx.core3.http.handler.Handler(a.getCore()) {  
            @Inject RequestController ctr;
            
            @Override
            public void handle() {
                String encrypted = WXTools.getBearerToken(ctr.getRequest());
                Token token = null;
                try{
                    token =  WXTools.mapper.readValue(a.getEncrypter().decrypt( URLDecoder.decode(encrypted, "UTF-8") ), 
                                                      Token.class);
                    if(new Date().getTime() > token.getValidTo()){
                        ctr.fail(new AuthenticationException("token expired"));                          
                    }
                }catch(Exception ex){ 
                    logger.error("invalid token",ex);
                    ctr.fail(new AuthenticationException("invalid token"));     
                    return;
                }                  
                final Token finalToken = token; //Oo                
                //resolve User Object for future handlers
                ctr.addModule(new AbstractModule(){
                                @Override
                                protected void configure() {
                                    bind(User.class).toInstance(finalToken.getUser());
                                }           
                });                                                
                ctr.next();
            }
        };
    }

    @Override
    public Handler<RoutingContext> setupAfterTrait(Application a) throws Exception {
        return null;
    }
    
}
