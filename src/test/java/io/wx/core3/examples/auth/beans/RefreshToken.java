/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.examples.auth.beans;

import io.wx.core3.common.WXTools;
import io.wx.core3.security.Encrypter;
import java.security.SignatureException;

/**
 *
 * @author moritz
 */
public class RefreshToken {
    private String clientId;
    private String clientSecret;
    private User user;
            

    
    public User getUser() {
        return user;
    }


    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public RefreshToken setUser(final User value) {
        this.user = value;
        return this;
    }


    public RefreshToken setClientId(final String value) {
        this.clientId = value;
        return this;
    }

    public RefreshToken setClientSecret(final String value) {
        this.clientSecret = value;
        return this;
    }

    
    

    

    
    
}
