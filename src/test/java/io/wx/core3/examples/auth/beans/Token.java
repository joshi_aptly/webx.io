/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.examples.auth.beans;
import io.wx.core3.common.WXTools;
import io.wx.core3.security.Encrypter;
import java.security.SignatureException;

/**
 * bean representation of a signed token 
 * 
 * @author moritz
 */
public class Token {
    private User user;
    private long validTo;

    public User getUser() {
        return user;
    }

    public Long getValidTo() {
        return validTo;
    }

    public Token setUser(final User value) {
        this.user = value;
        return this;
    }


    public Token setValidTo(final long value) {
        this.validTo = value;
        return this;
    }
    
    
    
}
