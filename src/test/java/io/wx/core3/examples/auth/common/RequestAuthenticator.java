/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.examples.auth.common;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.oauth2.AccessToken;
import io.vertx.ext.auth.oauth2.OAuth2Auth;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * 
 * @author moritz
 */
public class RequestAuthenticator {
    private static final Logger logger = LogManager.getLogger(RequestAuthenticator.class);  
    private final OAuth2Auth auth;
    private final JsonObject tokenconfig;
    

    public RequestAuthenticator(OAuth2Auth auth, JsonObject tokenconfig) {
        this.auth = auth;
        this.tokenconfig = tokenconfig;
    }
    
    
    public void doRequest(HttpClientRequest request, Future future){
        doRequest(request, future, null);
    }
    
    
    public void doRequest(HttpClientRequest request, Future future , AccessToken accessToken){
        doRequest(request, future, accessToken, true);
    }
   
    public void doRequest(HttpClientRequest request, Future future, AccessToken accessToken, boolean withRetry){       
        if(accessToken != null && !accessToken.expired()){
            handleRequest(request, future, accessToken, withRetry);
        }
        else if( (accessToken != null && accessToken.expired()) && !withRetry ){
            //access-token is expired and we dont want to retry again
            future.fail("expired token"); //@todo implement exception :D
        }
        else{
            //@todo implement refresh token
            auth.getToken(tokenconfig, (AsyncResult<AccessToken> e) -> {
                if(e.failed()){
                    future.fail(e.cause());
                }
                else{
                    doRequest(request, future, e.result(), false);
                }
            });
        }
    }
    
    
    
    private void handleRequest(HttpClientRequest request, Future future, AccessToken accessToken, boolean withRetry){
        request.headers().add("Authorization", "Bearer "+accessToken.principal().getString("access_token"));       
        request.handler((HttpClientResponse r) -> {
            if(r.statusCode() > 399 && r.statusCode()<500){
                if(withRetry){
                    logger.info("got auth error, retrying : "+r.statusCode()+":"+r.statusMessage());
                    doRequest(request, future, null, false);
                }
            }else{
                future.complete(r);
            }
        });    
        request.exceptionHandler((Throwable ex) -> {
            future.fail(ex);
        });    
        request.end();   
    }
    
    
    
}
