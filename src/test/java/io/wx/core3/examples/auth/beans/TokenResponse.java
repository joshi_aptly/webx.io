/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.examples.auth.beans;

/**
 *
 * @author moritz
 */
public class TokenResponse {
    private String access_token;
    private Integer expires_in;
    private String refresh_token;
    private String redirect_url;
    private long issued_at;

    public long getIssued_at() {
        return issued_at;
    }

    public String getAccess_token() {
        return access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public String getRedirect_url() {
        return redirect_url;
    }

    public Integer getExpires_in() {
        return expires_in;
    }

    public TokenResponse setAccess_token(final String value) {
        this.access_token = value;
        return this;
    }

    public TokenResponse setExpires_in(final Integer value) {
        this.expires_in = value;
        return this;
    }

    public TokenResponse setRefresh_token(final String value) {
        this.refresh_token = value;
        return this;
    }

    public TokenResponse setRedirect_url(final String value) {
        this.redirect_url = value;
        return this;
    }

    public TokenResponse setIssued_at(final long value) {
        this.issued_at = value;
        return this;
    }

    


    
    
}
