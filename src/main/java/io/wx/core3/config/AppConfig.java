/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.config;

import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.templ.TemplateEngine;
import io.wx.core3.http.routing.StaticRoute;
import java.util.List;
import java.util.Map;

/**
 * 
 * webx configuration object used by BasicConfigurator.java , CloudConfigurator.java
 *
 * @author moritz
 */
public interface AppConfig {
    
    public enum Stage {
        LOCAL, DEV, QA, DEFAULT, OTHER, TEST
    }
    
    AppConfig.Stage getStage();
    
    AppConfig setStage(Stage stage);
    
    String getHost();

    Integer getPort();
      
    boolean isSESSIONS_ALWAYS();
    
    AppConfig setSESSIONS_ALWAYS(boolean SESSIONS_ALWAYS);
    
    String getApplicationSecret();
    
    AppConfig setApplicationSecret(String applicationSecret);
  
    Map<String, Class> getModuleController();

    AppConfig setModuleController(Map<String, Class> mounts);
    
    /****
     * 
     * @param sessionTTL session tl in SECODS
     * @return 
     */
    AppConfig setSessionTTL(int sessionTTL);
    
    /****
     * get session ttl in SECODS
     * @return session ttl in seconds
     */
    int getSessionTTL();
    
    int getInstanceCount();
    
    AppConfig setInstanceCount(int instanceCount); 

    Boolean isDEVMODE();
    
    AppConfig setDEVMODE(Boolean DEVMODE);
    
    String getSSLPassword();

    String getSSLTrustStorePassword();

    Boolean isSSL();

    AppConfig setHost(String host);

    AppConfig setPort(Integer port);

    AppConfig setSSL(Boolean SSL);

    AppConfig setSSLPassword(String SSLPassword);

    AppConfig setSSLTrustStorePassword(String SSLTrustStorePassword);
    
    
    JsonObject getAddonCredentials();
    
    AppConfig setAddonCredentials(JsonObject addonCredentials);
    
    HttpClientOptions getHttpClientOptions();
    
    void setHttpClientOptions(HttpClientOptions httpClientOptions);    
    
    AppConfig setTemplateEngine(TemplateEngine templateEngine);
    
    TemplateEngine getTemplateEngine();
    
    List<String> getTemplateRoutes();
    
    AppConfig setTemplateRoutes(List<String> routes);
    
    List<StaticRoute> getPublicRoutes();
    
    AppConfig setPublicRoutes(List<StaticRoute> routes);
}
