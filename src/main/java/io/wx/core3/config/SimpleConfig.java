/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.config;

import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.templ.MVELTemplateEngine;
import io.vertx.ext.web.templ.TemplateEngine;
import io.wx.core3.common.WXTools;
import io.wx.core3.http.routing.StaticRoute;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * default configuration pojo for webx
 * 
 * @author moritz
 */
public class SimpleConfig implements AppConfig{
    private Integer port = 8080;
    private String host = "0.0.0.0";
    private Boolean SSL = false;
    private String SSLPassword = "wibble";
    private String SSLTrustStorePassword = SSLPassword;
    private Boolean DEVMODE = true;
    public int instanceCount = 1;
    private boolean SESSIONS_ALWAYS = false;
    private int sessionTTL = 120*60;
    private String applicationSecret = "thebestsecretkeyever!.-?013";
    private String sessionCacheName = "webxsessions";
    private JsonObject addonCredentials = null;
    private HttpClientOptions httpClientOptions  = new HttpClientOptions().setConnectTimeout(5000);    
    private Stage stage = Stage.DEFAULT; //lets say this is always default, if not resolved 
    private TemplateEngine templateEngine;
    private Map<String, Class> mounts = new HashMap<>();
    private List<String> templateRoutes = new ArrayList(){
        { add(".+\\.template"); }
    };
    private  List<StaticRoute> publicRoutes = new ArrayList<StaticRoute>() {
         { 
             add(new StaticRoute("/public/*", "public"));
             add(new StaticRoute(".+\\.htm", "public", true));
         }
    };
       
    @Override
    public Stage getStage() {
       return stage;
    }

    @Override
    public AppConfig setStage(Stage stage) {
        this.stage = stage;
        return this;
    }    
    
    
    public JsonObject getAddonCredentials() {
        if(addonCredentials == null){
            JsonObject loadCreds = WXTools.loadCredentials("./cfg/addon.creds");
            addonCredentials = (loadCreds != null) ? loadCreds : new JsonObject();
        }
        return addonCredentials;
    }

    public AppConfig setCredentials(JsonObject addonCredentials) {
        this.addonCredentials = addonCredentials;
        return this;
    }


    @Override
    public boolean isSESSIONS_ALWAYS() {
        return SESSIONS_ALWAYS;
    }

    @Override
    public AppConfig setSESSIONS_ALWAYS(boolean SESSIONS_ALWAYS) {
        this.SESSIONS_ALWAYS = SESSIONS_ALWAYS;
        return this;
    }
 
    
    @Override
    public String getApplicationSecret() {
        return applicationSecret;
    }

    @Override
    public AppConfig setApplicationSecret(String applicationSecret) {
        this.applicationSecret = applicationSecret;
        return this;
    }
    
    
    @Override
    public int getSessionTTL() {
        return sessionTTL;
    }

    @Override
    public AppConfig setSessionTTL(int sessionTTL) {
        this.sessionTTL = sessionTTL;
        return this;
    }
  
    
    @Override
    public int getInstanceCount() {
        return instanceCount;
    }

    @Override
    public AppConfig setInstanceCount(int instanceCount) {
        this.instanceCount = instanceCount;
        return this;
    }

    
    @Override
    public Boolean isDEVMODE() {
        return DEVMODE;
    }

    @Override
    public AppConfig setDEVMODE(Boolean DEVMODE) {
        this.DEVMODE = DEVMODE;
        return this;
    }

  
    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public AppConfig setPort(Integer port) {
        this.port = port;
        return this;
    }


    @Override
    public String getHost() {
        return host;
    }


    @Override
    public AppConfig setHost(String host) {
        this.host = host;
        return this;
    }


    @Override
    public Boolean isSSL() {
        return SSL;
    }


    @Override
    public AppConfig setSSL(Boolean SSL) {
        this.SSL = SSL;
        return this;
    }


    @Override
    public String getSSLPassword() {
        return SSLPassword;
    }


    @Override
    public AppConfig setSSLPassword(String SSLPassword) {
        this.SSLPassword = SSLPassword;
        return this;
    }

    @Override
    public String getSSLTrustStorePassword() {
        return SSLTrustStorePassword;
    }

    @Override
    public AppConfig setSSLTrustStorePassword(String SSLTrustStorePassword) {
        this.SSLTrustStorePassword = SSLTrustStorePassword;
        return this;
    }
    
    public HttpClientOptions getHttpClientOptions() {
        return httpClientOptions;
    }

    public void setHttpClientOptions(HttpClientOptions httpClientOptions) {
        this.httpClientOptions = httpClientOptions;
    }    

    @Override
    public TemplateEngine getTemplateEngine() {
      if(templateEngine == null){
        templateEngine = MVELTemplateEngine.create();
      }
      return templateEngine;
    }

    @Override
    public AppConfig setTemplateEngine(TemplateEngine templateEngine) {
      this.templateEngine = templateEngine;
      return this;
    }

    @Override
    public List<String> getTemplateRoutes() {
        return this.templateRoutes;
    }

    @Override
    public AppConfig setTemplateRoutes(List<String> routes) {
        this.templateRoutes = routes;
        return this;
    }
    
    @Override
    public List<StaticRoute> getPublicRoutes() {
        return this.publicRoutes;
    }

    @Override
    public AppConfig setPublicRoutes(List<StaticRoute> routes) {
        this.publicRoutes = routes;
        return this;
    }

    @Override
    public AppConfig setAddonCredentials(JsonObject addonCredentials) {
        this.addonCredentials = addonCredentials;
        return this;
    }

        
    @Override
    public Map<String, Class> getModuleController(){
        return this.mounts;
    }

    @Override
    public AppConfig setModuleController(Map<String, Class> mounts){
        this.mounts = mounts;
        return this;
    }
    



}   
