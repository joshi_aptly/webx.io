/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.di.l4j;

import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import java.lang.reflect.Field;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;


/**
 *
 * @author moritz
 */
public class Log4jTypeListener implements TypeListener{
    private LoggerContext ctx;
    
    public Log4jTypeListener(LoggerContext ctx){
        super();
        this.ctx = ctx;
    }
    
    
     public <T> void hear(TypeLiteral<T> typeLiteral, TypeEncounter<T> typeEncounter) {
            Class<?> clazz = typeLiteral.getRawType();
            while (clazz != null) {
              for (Field field : clazz.getDeclaredFields()) {
                if (field.getType() == Logger.class && field.isAnnotationPresent(InjectLogger.class)) {
                  typeEncounter.register(new LF4JMemberInjector<T>(field, ctx));
                }
              }
              clazz = clazz.getSuperclass();
            }
    }   
    
}
