/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.di.l4j;

import com.google.inject.MembersInjector;
import java.lang.reflect.Field;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;

/**
 *
 * @author moritz
 */
public class LF4JMemberInjector<T> implements MembersInjector<T> {
   private final Field field;
   private final Logger logger;
 
    public LF4JMemberInjector(Field field, LoggerContext ctx) {
        this.field = field;
        this.logger = ctx.getLogger(field.getDeclaringClass().getName());
        field.setAccessible(true);
    }
 
    public void injectMembers(T t) {
        if( java.lang.reflect.Modifier.isStatic(field.getModifiers()) ) {
            try {
                //for static members we only set the logger if its empty:
                if (field.get(t) != null){
                    return;
                }
            } catch (IllegalArgumentException | IllegalAccessException  ex) {
                throw new RuntimeException("cant get value of static Field",ex);
            }
        }        
        try {
            field.set(t, logger);
        } catch (IllegalAccessException | IllegalArgumentException e) {
            throw new RuntimeException(e);
        }
    }    
}
