/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http;

import io.wx.core3.http.traits.TraitConf;
import io.vertx.core.http.HttpMethod;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
@Retention( RetentionPolicy.RUNTIME )
@Target(ElementType.METHOD)
public @interface RequestMapping {
    HttpMethod method() default HttpMethod.GET;
    String path() default "";
    String regex() default "";   
    String consumes() default "";   
    String[] produces() default {};   
    int order() default 0;
    TraitConf[] traits() default {};
    HttpStatus httpStatus() default HttpStatus.OK;
}
