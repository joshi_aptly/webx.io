package io.wx.core3.http;

import io.wx.core3.common.WXTools;
import com.google.inject.Injector;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.net.JksOptions;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.TemplateHandler;
import io.vertx.ext.web.templ.HandlebarsTemplateEngine;
import io.vertx.ext.web.templ.JadeTemplateEngine;
import io.vertx.ext.web.templ.MVELTemplateEngine;
import io.vertx.ext.web.templ.TemplateEngine;
import io.vertx.ext.web.templ.ThymeleafTemplateEngine;
import io.wx.core3.config.AppConfig.Stage;
import io.wx.core3.http.app.Application;
import io.wx.core3.xml.XMLSchemaResolver;
import io.wx.core3.http.handler.DefaultFailureHandler;
import io.wx.core3.http.handler.DefaultLastHandler;
import io.wx.core3.http.routing.RoutingEngine;
import io.wx.core3.http.routing.StaticRoute;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import org.apache.commons.beanutils.FluentPropertyBeanIntrospector;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.FileTemplateResolver;
/**
 * webx.http.Core: Bootstraps and configures the http server
 *
 * @author moritz
 */
public class Core{
    private static final Logger logger = LogManager.getLogger(Core.class);
    private final Application app;
    private final Vertx vertx;
    //private final PlatformManager platformManager;
    private final List<HttpServer> server = new ArrayList<>();    
    private final Reflections reflections;
    private RoutingEngine router;
    private File publicDirectory; 
    private JAXBContext jaxbContext;
    private Map<String, Set<Class>> filter = new HashMap<>();
    
    
    /***
     * initializes the core with give application
     * 
     * @param a your application
     * @throws JAXBException
     * @throws IOException 
     */
    public Core(Application a) throws JAXBException, IOException  {
        this.app = a;     
        /* scan application path for reflections */
        this.reflections = new Reflections(this.app.getApplicationPath());
        this.vertx = Vertx.vertx(); //this.platformManager.vertx
        
        /* make beanutils fluent: */
        PropertyUtils.addBeanIntrospector(new FluentPropertyBeanIntrospector());
        /* set vertx logger to Log4j */
        System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.Log4jLogDelegateFactory");
         
        /* init jaxb */
        try {
            this.jaxbContext = WXTools.initJAXB(this.reflections);
        } catch (JAXBException ex) {
            throw new JAXBException("Cant init JAXB :| please take a look",ex);
        }
    }
 
    
    
    /**
     * start the http servers
     * @throws IOException 
     */
    public void start() throws Exception{
        String proto = this.app.getConfig().isSSL() ? "https" : "http";

        logger.info("Hello "+this.app.getClass().getName()+" !");
        init();
        logger.info("> starting "+this.server.size()+" HTTP server on "+proto+"://"+this.app.getConfig().getHost()+":"+this.app.getConfig().getPort());
        List<Future> httpServerFutures = new ArrayList<>();
        for(HttpServer srv : this.server){
            Future<HttpServer> httpServerFuture = Future.future();
            httpServerFutures.add(httpServerFuture);
            srv.listen(this.app.getConfig().getPort(), this.app.getConfig().getHost(), httpServerFuture.completer());
        }    
        CompositeFuture.all(httpServerFutures).setHandler((handler) -> app.postStart(handler));        
    }
    

    /***
     * init core
     * @throws IOException 
     */    
    private void init() throws Exception{
        if(app.getModules().size()>0){
             logger.info("> DI-Modules:"); 
        }
        for(String moduleName : app.getModules().keySet()){
            logger.info(" >> "+moduleName);
        } 
        /* setup routing */
        logger.info("> preparing routes: ");
        router = new RoutingEngine(this);        
        logger.info(" >> creating public routes");
        bindPublicRoutes();
        logger.info(" >> creating controller routes:");
        router.initController(this.app.getApplicationPath());        
        
        /* setup defaultLasthandler and failurehandler
        *  @todo enable feature to write own last handlers? 
        *  @todo implement a way to specifiy failure handlers
        */
        router.getRouter().route().order(999999999).handler(new DefaultLastHandler(this))
                                                   .failureHandler(new DefaultFailureHandler());
        /* Add TemplateEngine */
        configureTemplateEngine();
                
        /* generate jaxb schema into public directory */
        logger.info("> generating xml schema in /public/schema1.xsd");
        this.jaxbContext.generateSchema(new XMLSchemaResolver(this.publicDirectory));

        
        /* init http server(s) */
        HttpServer srv;
        for(int i=0;i<app.getConfig().getInstanceCount();i++){
            logger.info("> preparing http server "+(i+1));
            HttpServerOptions serverOptions = new HttpServerOptions();
            //configure https
            if(this.app.getConfig().isSSL()){
                serverOptions.setSsl(true);
                if(this.app.getSSLKeystore()==null || !this.app.getSSLKeystore().exists()){                   
                     logger.error(" ! cant activate ssl, keystore not found !");                   
                }
                else{
                    serverOptions.setKeyStoreOptions(new JksOptions()
                                                    .setPath(this.app.getSSLKeystore().getAbsolutePath())
                                                    .setPassword(this.app.getConfig().getSSLPassword()));
                    if(this.app.getSSLTrustStore()!=null && this.app.getSSLTrustStore().exists()){
                        serverOptions.setTrustStoreOptions(new JksOptions()
                                                     .setPath(this.app.getSSLTrustStore().getAbsolutePath())
                                                     .setPassword(this.app.getConfig().getSSLTrustStorePassword()));                     
                    }
                }
            }
            srv = this.vertx.createHttpServer(serverOptions); 
            srv.requestHandler(router.getRouter()::accept);
            this.server.add(srv);
        }  
    }
   
    /***
     * binds the public routes for file serving
     * @throws IOException 
     */
    private void bindPublicRoutes() throws IOException {
        List<StaticRoute> publicRoutes = getApplication().getConfig().getPublicRoutes();
        File directory = null;
      
        for(StaticRoute route : publicRoutes){
            logger.info(" >> serving files matching to " + route.getPath() + " from " + route.getDirectory());
            directory = new File("./" + route.getDirectory());
        
            if(!directory.isDirectory()){
                throw new IOException("please create "+directory.getAbsolutePath());
            } 
            if (route.isRegexp()) {
                router.getRouter().getWithRegex(route.getPath()).handler(StaticHandler.create().setWebRoot(route.getDirectory()).setIncludeHidden(false));            
            }
            else {
                router.getRouter().route(route.getPath()).handler(StaticHandler.create().setWebRoot(route.getDirectory()).setIncludeHidden(false));            
            }
        }
    }

    
    /**
     * configure templating
     */
    private void configureTemplateEngine(){       
      TemplateEngine engine = getApplication().getConfig().getTemplateEngine();
      List<String> routes = getApplication().getConfig().getTemplateRoutes();
      
      if(engine != null && routes != null && routes.size()>0){
        TemplateHandler handler = TemplateHandler.create(engine);
        //configure engines
        if (engine instanceof MVELTemplateEngine){     
          if (getApplication().getConfig().getStage() == Stage.LOCAL)
          {
            ((MVELTemplateEngine) engine).setMaxCacheSize(0);
          }
        }
        else if (engine instanceof JadeTemplateEngine){
          if (getApplication().getConfig().getStage() == Stage.LOCAL)
          {
            ((JadeTemplateEngine) engine)
              .setMaxCacheSize(0)
              .getJadeConfiguration()
              .setCaching(false);
          }
        }
        else if (engine instanceof HandlebarsTemplateEngine){   
          if (getApplication().getConfig().getStage() == Stage.LOCAL)
          {
            ((HandlebarsTemplateEngine) engine).setMaxCacheSize(0);
          }
        }
        else if (engine instanceof ThymeleafTemplateEngine){
          ThymeleafTemplateEngine eng = (ThymeleafTemplateEngine) engine;                              
          FileTemplateResolver templateResolver = new FileTemplateResolver();
          if (getApplication().getConfig().getStage() == Stage.LOCAL){
            templateResolver.setCacheable(false);
          }
          templateResolver.setSuffix(".tlf");
          templateResolver.setTemplateMode(TemplateMode.HTML);
          templateResolver.setCharacterEncoding("UTF8");
          templateResolver.setCheckExistence(true);
          eng.getThymeleafTemplateEngine().setTemplateResolver(templateResolver);          
        }
        /* bind template routes */
        for(String route : routes){
            router.getRouter().getWithRegex(route).handler(handler);            
        }
      }
    }
    
    public Application getApplication(){
        return this.app;
    }

    public Vertx getVertx() {
        return vertx;
    }

    public List<HttpServer> getServer() {
        return server;
    }

    public RoutingEngine getRoutingEngine() {
        return router;
    }


    public File getPublicDirectory() {
        return publicDirectory;
    }


    public Injector getRootInjector() {
        return app.getRootInjector();
    }

    public JAXBContext getJaxbContext() {
        return jaxbContext;
    }

    public Reflections getReflections() {
        return reflections;
    }

    public Map<String, Set<Class>> getFilter() {
        return filter;
    }
    
}
