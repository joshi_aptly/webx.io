/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http.app;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Provides;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.ext.web.templ.TemplateEngine;
import io.wx.core3.di.l4j.InjectLogger;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.Configurator;
import io.wx.core3.security.Encrypter;
import io.wx.core3.security.EncrypterImpl;
import io.wx.core3.http.Core;
import io.wx.core3.common.WXTools;
import io.wx.core3.di.l4j.Log4jModule;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.apache.bval.guice.ValidationModule;
import org.apache.logging.log4j.Logger;

/**
 *
 *  @todo rewrite application interface for engine 3
 * @author moritz
 */
public abstract class AbstractApplication implements Application{
    private boolean isInitialized = false; //the initialized means - the DI Modules are loadet and available
    private Core c;
    protected Encrypter encrypter;
    private Injector rootInjector;        //the DI rootinjector     
    private  Map<String, ? extends Module> modules = new HashMap<>();
    
    /* required stuff resolved by DI */
    @InjectLogger
    public Logger logger;
    @Inject
    private AppConfig config; 
    
    
    
    /***
     * Creates a new AbstractApplication Instance
     * @param cfg
     * @throws Exception 
     */
    public AbstractApplication(AppConfig cfg) throws Exception{
        this(cfg, Collections.emptyList().toArray(new Configurator[0]));
    }
    
    
    /***
     * Create a new AbstractApplication with the given Configurators
     * @param cfg
     * @param configs
     * @throws Exception 
     */
    public AbstractApplication(AppConfig cfg, Configurator... configs) throws Exception{
        c = new Core(this);
        //you can override modules in your configurator, but webx provides some defaults:
        Map basics = getModules();
        final Application myApp = this;
        basics.put("vertx", new AbstractModule(){
            @Override
            protected void configure() {
                bind(Vertx.class).toInstance(c.getVertx());
            }           
        });
        basics.put("config", new AbstractModule(){
            @Override
            protected void configure() {
                bind(AppConfig.class).toInstance(cfg);
            }           
        });
        basics.put("log4j", new Log4jModule());
        //templating
        basics.put("templating", new AbstractModule() {
            @Override
            protected void configure() {
               bind(TemplateEngine.class).toInstance(cfg.getTemplateEngine());
            }
        });  
        //create a default httpclient based on the cfg, override if you need something else:
        basics.put("httpclient", new AbstractModule() {
            @Override
            protected void configure() { }
            @Provides
            HttpClient getHttpClient(){
                return c.getVertx().createHttpClient(cfg.getHttpClientOptions());
            } 
        });        
        //apply configurators:
         for(Configurator c : configs){
            c.setAppConfig(cfg);
            c.doConfig(basics);
        }                
    }
    


    
    /**
     * initializes the application, creates the core and the DI. after this happens, new entries in this.modules will not affect the di anymore
     * @return false if the application was already initialized, true its just happend
     */
    @Override
    public boolean initializeDI(){
        if(isInitialized){
            return false;
        }
        else{
            final Application app = this;
            Map modules = getModules();
            modules.put("Application", new AbstractModule() {
                @Override
                protected void configure() {
                    bind(Application.class).toInstance(app);
                }
            });
            modules.put("Bval", new ValidationModule());
            rootInjector = Guice.createInjector(getModules().values());
            rootInjector.injectMembers(this);//weeeee     
            isInitialized = true;
            return isInitialized;
        }
    }

    
    /*
    * starts the server, initializes the DI if it has not happend yet
    */
    @Override
    public AbstractApplication start() throws Exception{
        initializeDI();
        getCore().start();
        return this;
    }   
    
    /*
    * This function get called after the server(s) are all up and running.
    * At this point, the DI is available
    */
    @Override
    public void postStart(AsyncResult<CompositeFuture> ar){
        if (ar.succeeded()) {
           logger.info("Servers up and running");
        } else {
           logger.error("Error starting Servers " + ar.cause().getMessage());
        }
    }

    /*
    *  idles the application so that the JVM doesnt shut down 
    */
    @Override
    public void idle() throws InterruptedException{
        synchronized(this){
            this.wait();
        }
    } 
    
    


    /** 
     * returns the webx core
     * 
     * @return
     */
    @Override
    public final Core getCore(){
        return c;
    }
    
    

    
    /***
     * smart getter for encrypter, creates if not yet exists
     * @return
     * @throws Exception 
     */
    @Override
    public Encrypter getEncrypter() throws Exception{
        if(encrypter==null){
            if("thebestsecretkeyever!.-?013".equals(config.getApplicationSecret())){
                logger.error("!! YOU ARE USING THE DEFAULT APPLICATION SECRET TO ENCRYPT SESSION COOKIES !! please override getApplicationSecret() in your application");
            }            
            encrypter = new EncrypterImpl(config.getApplicationSecret(), "mySaltIsThisImporant?");
        }
        return encrypter;
    }
    
    
    /***
     * returns the ssl truststore
     * @return 
     */
    @Override
    public File getSSLTrustStore(){
        return null;
    }

    
    /**
     * returns the SSL keystore
     * @return 
     */
    @Override
    public File getSSLKeystore(){
        File f = new File("./cfg/server-keystore.jks");
        if(f.exists()){
            return f;
        }
        else{
            logger.error("missing keystore file "+f.getAbsolutePath());
        }
        return null;
    }

    
    

   
    /**
     * define the handler used any requests which doesnt match a route 
     * @return 
     */
    @Override
    public Handler<HttpServerRequest> getNotFoundHandler(){
        return new Handler<HttpServerRequest>() { 
            public void handle(HttpServerRequest req) { 
                WXTools.send404Page(req);
            } 
        };
    }
    
    
    /**
     * define your guice modules here
     */
    @Override
    public Map<String, ? extends Module> getModules(){
        return modules;
    }
    
    
    /***
     * returns the root injector
     * @return 
     */
    @Override
    public Injector getRootInjector() {
        return rootInjector;
    }


    /***
     * returns the Application config
     * @return 
     */
    @Override
    public AppConfig getConfig() {
        return config;
    }

    
    /***
     * sets the Application 
     * @param config 
     */
    @Override
    public void setConfig(AppConfig config) {
        this.config = config;
    }
           
    
    
}
