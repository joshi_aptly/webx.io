/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http.handler;

import com.google.inject.Injector;
import com.google.inject.Module;
import io.vertx.ext.web.RoutingContext;
import io.wx.core3.di.RequestControllerModule;
import io.wx.core3.http.Core;
import io.wx.core3.http.RequestControllerImpl;
import io.wx.core3.http.RequestController;
import java.util.HashSet;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * Abstract class to implement Vertx-Handler with DI Support 
 * 
 * @author moritz
 */
public abstract class Handler implements io.vertx.core.Handler<RoutingContext>{
    private static final Logger logger = LogManager.getLogger(Handler.class);
    private final Core core;
    private Injector injector=null;
    
    /** 
     * be careful, no dependency injection in constructor
     * @param c 
     */
    public Handler(Core c){
        this.core = c;        
    }
    
    
    @Override
    public final void handle(RoutingContext e) {
        if( e.data().get("_isdebug") == null){
            e.data().put("_isdebug", core.getApplication().getConfig().isDEVMODE());
        }            
        RequestController r = new RequestControllerImpl(e, core);
        Set<Module> modules = new HashSet<>();
        modules.addAll(r.getModules());
        modules.add(new RequestControllerModule(r));
        injector = core.getRootInjector().createChildInjector(modules); 
        this.injector.injectMembers(this);
        handle();
    }
    
    public abstract void handle();

    public Injector getInjector() {
      return injector;
    }
}
