/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor
 */
package io.wx.core3.http.routing;

import io.vertx.core.http.HttpMethod;
import io.wx.core3.http.Core;
import io.wx.core3.http.session.Stateful;
import io.wx.core3.http.RequestMapping;
import static io.wx.core3.http.HttpMethodUtils.EMPTY_PATH;
import io.wx.core3.http.HttpStatus;
import io.wx.core3.http.traits.TraitConf;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * The blueprint class/method mapped for http request handling
 * 
 * @author moritz
 */
public class ControllerBlueprint {
    private static final Logger logger = LogManager.getLogger(ControllerBlueprint.class);
    
    private final Class impl;
    private final Method method;
    private final Core core;
    private final String path;
    private final String consumes;
    private final String[] produces;
    private final Integer order;
    private final HttpMethod httpMethod;
    private final HttpStatus httpStatus;
    private String regex;
    private TraitConf[] traits;
    private final String controllerName;        //name of class + method
    //private Boolean sessionRequired;
    private final String statefulID;   //implementation id for stateful handlung 
    private final List<Field> statefulFields; //stateful handling for fields    
    
    
    /****
     * 
     * @param impl the implementing class
     * @param method the method taking care of you stuff
     * @param core the core running the appication
     * @param annotation the requestmapping
     */
    public ControllerBlueprint(Class impl, Method method, Core core, RequestMapping annotation) {
        this.impl = impl;
        this.method = method;
        this.core = core;
        this.path = annotation.path();
        this.consumes = annotation.consumes();
        this.produces = annotation.produces();
        this.order = annotation.order();
        this.httpMethod = annotation.method();
        this.httpStatus = annotation.httpStatus();
        this.regex = annotation.regex();
        if ("".equals(this.regex))
        {
           this.regex = EMPTY_PATH;  
        }
        this.traits = annotation.traits();
        this.controllerName = impl.getName() + "." + method.getName();        
        Stateful stateful = (Stateful) impl.getAnnotation(Stateful.class);
        //stateful controller?
        if(stateful != null) {
            statefulID = impl.getName();
            //sessionRequired = true;
            statefulFields = null;
        }
        //stateful fields?
        else{
            statefulID = null;
            statefulFields = new ArrayList<>();
            for(Field field : impl.getFields()) {
                if(field.getAnnotation(Stateful.class) != null) {
                    statefulFields.add(field);
                }
            }
        }   
    }

    public Core getCore() {
        return core;
    }
    
    public Integer getOrder() {
      return order;
    }

    
    public Class getImpl() {
        return impl;
    }

    public Method getMethod() {
        return method;
    }

    public String getPath() {
        return path;
    }

    public String getConsumes() {
      return consumes;
    }

    public String[] getProduces() {
      return produces;
    }

    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

    public String getControllerName() {
        return controllerName;
    }

    public String getStatefulID() {
        return statefulID;
    }

    public List<Field> getStatefulFields() {
        return statefulFields;
    }

    public String getRegex() {
      return regex;
    }

    public TraitConf[] getTraits() {
      return traits;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    @Override
    public String toString() {
        return "ControllerBlueprint{ httpMethod=" + httpMethod + ", " + "httpStatus=" + httpStatus + ", path=" + path + ", consumes=" + consumes + ", produces=" + produces + ", controllerName=" + controllerName + ", statefulID=" + statefulID + ", statefulFields=" + statefulFields + '}'; //+ ", sessionRequired=" + sessionRequired
    }

    
    /***
     * describe the blueprint as string
     * @return 
     */
    public String describe(){
        String result = httpStatus.value() + " " + httpMethod + " " + path + " => " + controllerName +" (";// sessionRequired=" + sessionRequired+", ";
        if(statefulID != null){
            result += " statefulID=" + statefulID;
        }
        if(statefulFields != null && statefulFields.size() > 0){
            result += ", statefulFields=[";
            for(int i=0; i<statefulFields.size();i++){
                if(i > 0){
                    result += ",";
                }
                result += statefulFields.get(i).getName();
            }
            result += "]";
        }
        return result+" )";
    }
    
}
