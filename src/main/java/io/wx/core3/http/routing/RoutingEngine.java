/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http.routing;

import io.wx.core3.http.handler.ResourceInvocationHandler;
import io.vertx.core.Handler;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.wx.core3.http.Resource;
import io.wx.core3.http.Core;
import io.wx.core3.http.HttpMethodUtils;
import static io.wx.core3.http.HttpMethodUtils.EMPTY_PATH;
import io.wx.core3.http.routing.RoutingOrder.Order;
import io.wx.core3.http.traits.Trait;
import io.wx.core3.http.traits.TraitConf;
import io.wx.core3.http.traits.TraitFactory;
import java.lang.reflect.Method;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * annotation based http routing engine
 *
 * @author moritz, jose
 */
public class RoutingEngine {
    private static final Logger logger = LogManager.getLogger(RoutingEngine.class);
    private final Router router; //the main router
    private final Core core;    
    
    /***
     * 
     * @param core webx core object 
     */
    public RoutingEngine(Core core){
        this.core = core;
        this.router = Router.router(core.getVertx()); 
    }
    
    public Router getRouter() {
        return this.router;
    }
    
    public RoutingEngine initController(String classPath) throws Exception {    
        Set<Class<?>> annotated = core.getReflections().getTypesAnnotatedWith(Resource.class);        
        //core.getApplication().getConfig().getModuleController();
        //annotated.addAll(annotated)
        for (Class c : annotated) {
            Resource resource = (Resource) c.getAnnotation(Resource.class); 
            mountResource(this.router, null, resource, c);
        }
        for(String path : core.getApplication().getConfig().getModuleController().keySet()){
            Class controller =  core.getApplication().getConfig().getModuleController().get(path);
            Resource resource = (Resource) controller.getAnnotation(Resource.class); 
            mountResource(this.router, path, resource, controller);              
        }
        return this;
    }
        

    /****
     * 
     * @todo this implementation is still wierd...may cause i wanted to implement subresources but never did?
     * 
     * mounts a http resource
     * @param parentRouter
     * @param resource
     * @param c
     * @throws Exception 
     */
    private void mountResource(Router parentRouter, String prefix, Resource resource, Class c) throws Exception{
         if(prefix==null){
             prefix = "";
         }
         Router subRouter = Router.router(core.getVertx());  
         int traitsOrderForBeforeMethods = Order.TRAIT_METHOD_BEFORE.getValue();
         int traitsOrderForAfterMethods = Order.TRAIT_METHOD_AFTER.getValue();
        
         logger.info("- Resource: "+prefix+resource.path());
         addClassTraits(subRouter, resource);
      
         for (Method m : c.getMethods()) {           
             ControllerBlueprint blueprint;
             Route route;
             HttpMethodUtils http = new HttpMethodUtils(c, m, core);
             if (!http.IsHttpMethod())
             {
                if (http.IsHttpResource()) {
                   //add class traits here?@todo mount with "accept" here. add secial http method type "subresource!"
                   mountResource(subRouter, "", (Resource) m.getAnnotation(Resource.class), m.getReturnType());
                   continue; // sub locator not yet implemented
                }
                else{
                    continue;
                }
             }
            blueprint = http.getBlueprint();
            logger.info("    -> "+blueprint.describe());
            traitsOrderForBeforeMethods = addMethodTraits(true, subRouter,http, traitsOrderForBeforeMethods); //@todo one method call which does both?????????????
            traitsOrderForAfterMethods = addMethodTraits(false, subRouter,http, traitsOrderForAfterMethods);
            if("".equals(http.getBlueprint().getPath())){
                route = subRouter.routeWithRegex(http.getBlueprint().getHttpMethod(), http.getBlueprint().getRegex());
            }
            else{          
                route = subRouter.route(http.getBlueprint().getHttpMethod(), http.getBlueprint().getPath());                
            }
            if (http.getBlueprint().getOrder()> 0){    
                route = route.order(http.getBlueprint().getOrder());      
            }    
            
            //Setting consumes for the method            
            if ( !"".equals(http.getBlueprint().getConsumes()) ) {
              logger.info("      consumes " + http.getBlueprint().getConsumes());
              route.consumes(http.getBlueprint().getConsumes());
            }
            else if( !"".equals(resource.consumes())){
              logger.info("      consumes " + resource.consumes());
              route.consumes(resource.consumes());
            }
            
            //Setting produces for the method            
            if ( http.getBlueprint().getProduces().length > 0 ) {
              for (String produce : http.getBlueprint().getProduces())
              {
                logger.info("      produces " + produce);
                route.produces(produce);
              }
            }
            else {
              for (String produce : resource.produces()){
                route.produces(produce);
              }
            }
            route.handler(new ResourceInvocationHandler(blueprint));  
         }
         parentRouter.mountSubRouter(prefix+resource.path(), subRouter); //uuuuuuuuuuuuuh, no regex support?!?!?!? 
    }
  
      
  /***
   * add traits for a resource 
   * 
   * @param subRouter
   * @param resource
   * @throws Exception 
   */
  private void addClassTraits(Router subRouter, Resource resource) throws Exception {    
    int traitsBeforeOrder = Order.TRAIT_CLASS_BEFORE.getValue();
    int traitsAfterOrder = Order.TRAIT_CLASS_AFTER.getValue();
    for(TraitConf traitConf : resource.traits()){
      Class<? extends Trait> trait = traitConf.trait();
      try {
        Route route = null;
        Handler<RoutingContext> handlerBeforeTrait = TraitFactory.getBeforeHandlerTrait(trait, this.core.getApplication());
        if (handlerBeforeTrait != null)
        {
          logger.info("    adding ClassTrait Before " + trait.getSimpleName() + " with order " + traitsBeforeOrder);              
          route = subRouter.route().order(traitsBeforeOrder++);
          
          //consumes from resource has precedence
          if( !"".equals(resource.consumes())){
            logger.info("          consumes->" + resource.consumes());
            route.consumes(resource.consumes());

            if ( !"".equals(traitConf.consumes()) ) {
              logger.warn("  ignoring consumes configuration for "+trait.getSimpleName()+"  on  " + resource.path() + ". " + resource.consumes() +" will be used instead");
            }
          }
          else if ( !"".equals(traitConf.consumes()) ) {
            logger.info("          consumes->" + traitConf.consumes());
            route.consumes(traitConf.consumes());
          }
          
          //produces from resource have precedence
          if (resource.produces().length > 0)
          {
            for (String produce : resource.produces())
            {
              logger.info("          produces->" + produce);
              route.produces(produce);
            }
            
            if ( traitConf.produces().length > 0 ) {
              logger.warn("  ignoring produces configuration for "+trait.getSimpleName()+"  on  " + resource.path() + ". " + resource.produces() +" will be used instead");
            }
          }
          else {
            for (String produce : traitConf.produces())
            {
              logger.info("          produces->" + produce);
              route.produces(produce);
            }
          }
          
          route.handler(handlerBeforeTrait);
        }

        Handler<RoutingContext> handlerAfterTrait = TraitFactory.getAfterHandlerTrait(trait, this.core.getApplication());
        if (handlerAfterTrait != null)
        {
          logger.info("    adding ClassTrait After " + trait.getSimpleName() + " with order " + traitsAfterOrder);
          route = subRouter.route().order(traitsAfterOrder++).handler(handlerAfterTrait);
        }
        
      } catch (Exception ex) {
        logger.error("Error creating trait " + trait.getName(), ex);
        throw ex;
      }
    }
  }  
  
    /****
     * add traits for a method
     * 
     * @param isBeforeTraits
     * @param subRouter
     * @param http
     * @param traitsOrderForMethods
     */
    private int addMethodTraits(boolean isBeforeTraits, Router subRouter, HttpMethodUtils http, int traitsOrderForMethods) throws Exception {
      for(TraitConf traitConf : http.getBlueprint().getTraits()){      
          Class<? extends Trait> trait = traitConf.trait();
          try {
            Handler<RoutingContext> handlerTrait;
          
            if (isBeforeTraits)
            {
                handlerTrait = TraitFactory.getBeforeHandlerTrait(trait, this.core.getApplication());
            }
            else {
                handlerTrait = TraitFactory.getAfterHandlerTrait(trait, this.core.getApplication());
            }
            if ( handlerTrait != null)
            {
              logger.info("       adding MethodTrait " + trait.getSimpleName() + " with order " + traitsOrderForMethods);
              Route route;
              if("".equals(http.getBlueprint().getPath())){
                route = subRouter.routeWithRegex(http.getBlueprint().getHttpMethod(), EMPTY_PATH).order(traitsOrderForMethods++);
              }
              else{          
                route = subRouter.route(http.getBlueprint().getHttpMethod(), http.getBlueprint().getPath()).order(traitsOrderForMethods++); 
              }
              
              //consumes precedence: Blueprint(Method) -> Class -> Trait
              if( !"".equals(http.getBlueprint().getConsumes())){
                logger.info("            consumes->" + http.getBlueprint().getConsumes());
                route.consumes(http.getBlueprint().getConsumes());
                
                if ( !"".equals(traitConf.consumes()) ) {
                  logger.warn("  ignoring consumes configuration for " + trait.getSimpleName() + "  on  "+http.getBlueprint().getControllerName() + ". " + http.getBlueprint().getConsumes() +" will be used instead");
                }
              }
              else if ( !"".equals(http.getResource().consumes()) ) {
                logger.info("            consumes->" + http.getResource().consumes());
                route.consumes(http.getResource().consumes());
                
                if ( !"".equals(traitConf.consumes()) ) {
                  logger.warn("  ignoring consumes configuration for " + trait.getSimpleName() + "  on  "+http.getBlueprint().getControllerName() + ". " + http.getResource().consumes() +" will be used instead");
                }
              }
              else if ( !"".equals(traitConf.consumes()) ) {
                logger.info("            consumes->" + traitConf.consumes());
                route.consumes(traitConf.consumes());
              }
              
              
              //produces precedence: Blueprint(Method) -> Class -> Trait
              if( http.getBlueprint().getProduces().length > 0){
                for (String produce : http.getBlueprint().getProduces())
                {
                  logger.info("          produces->" + produce);
                  route.produces(produce);
                }
                
                if ( traitConf.produces().length > 0 ) {
                  logger.warn("  ignoring produces configuration for " + trait.getSimpleName() + "  on  "+http.getBlueprint().getControllerName() + ". " + http.getBlueprint().getProduces() +" will be used instead");
                }
              }
              else if ( http.getResource().produces().length > 0) {
                for (String produce : http.getResource().produces())
                {
                  logger.info("          produces->" + produce);
                  route.produces(produce);
                }
                
                if ( traitConf.produces().length > 0 ) {
                  logger.warn("  ignoring produces configuration for " + trait.getSimpleName() + "  on  "+http.getBlueprint().getControllerName() + ". " + http.getResource().produces() +" will be used instead");
                }
              }
              else{
                for (String produce : traitConf.produces())
                {
                  logger.info("          produces->" + produce);
                  route.produces(produce);
                }
              }
              
              route.handler(handlerTrait);
            }
          } catch (Exception ex) {
            logger.error("Error creating trait " + trait.getName(), ex);
            throw ex;
          }
      }
      return traitsOrderForMethods;
    }  
}
