/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.http.session;

/**
 *
 * @author moritz
 */
public class InvalidSessionCookieSignatureException extends Exception {

    /**
     * Creates a new instance of
     * <code>InvalidSessionCookieSignatureException</code> without detail
     * message.
     */
    public InvalidSessionCookieSignatureException() {
    }

    /**
     * Constructs an instance of
     * <code>InvalidSessionCookieSignatureException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public InvalidSessionCookieSignatureException(String msg) {
        super(msg);
    }
    
    public InvalidSessionCookieSignatureException(String msg, Throwable cause) {
        super(msg,cause);
    }
}
