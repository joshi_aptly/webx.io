package io.wx.core3.http.session;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.handler.SessionHandler;
import io.wx.core3.http.app.Application;
import io.wx.core3.http.cookies.CookieStore;
import io.wx.core3.common.WXTools;
import io.wx.core3.http.handler.Handler;
import io.wx.core3.http.RequestController;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Session handler for encrypted session cookies based on json
 * 
 * @author moritz
 */
public class CookieSessionHandler extends Handler implements SessionHandler{
    private static final Logger logger = LogManager.getLogger(CookieSessionHandler.class);    
    private String SESSION_COOKIE_NAME = "wxsessionid"; //this one is not the actuall name of the session, for session cookies the id actually is not important at all
    private long SESSION_TIMEOUT = SessionHandler.DEFAULT_SESSION_TIMEOUT;
    private boolean NAG_HTTPS = SessionHandler.DEFAULT_NAG_HTTPS;
    private boolean COOKIE_HTTP_ONLY_FLAG = SessionHandler.DEFAULT_COOKIE_HTTP_ONLY_FLAG;
    private boolean COOKIE_SECURE_FLAG = SessionHandler.DEFAULT_COOKIE_SECURE_FLAG;
    private final CookieSessionEncrypter scEncrypter;    
    @Inject public RequestController ctr;    
    
    
    public CookieSessionHandler(Application a) throws Exception {
        super(a.getCore());
        if(a.getConfig().getSessionTTL()>0){
            SESSION_TIMEOUT = a.getConfig().getSessionTTL()*1000;
        }
        scEncrypter = new CookieSessionEncrypter(a.getEncrypter(), SESSION_TIMEOUT);
    }
    
    @Override
    public void handle() {
        RoutingContext e = ctr.getRoutingContext();
        if( e.data().get("_cookiestore") == null ){            
            e.data().put("_cookiestore", new CookieStore(e.request()));
        }       
        CookieStore cookieStore = (CookieStore) e.data().get("_cookiestore"); //todo implement vertx cookies#*/
        String sessionID = cookieStore.getValue(SESSION_COOKIE_NAME);
        if (sessionID == null) {
            sessionID = WXTools.getBearerToken(e.request());
        }        
        Session session = getSessionFromCookie(sessionID, cookieStore);
        e.setSession(session);
        logger.debug("session "+session);
        //e.data().put("_session", session);        
        e.data().put("_sessionEncrypter", scEncrypter);       
        e.next();
    }

    
    /**
     * *
     * reads all signed data from the cookie store to the session
     *
     * @param id
     * @param cs
     * @return
     */
    public Session getSessionFromCookie(String id, CookieStore cs) {
        if(scEncrypter == null){
            logger.fatal("cant read session cookies. SessionCookieEncrypter is not initialized. Please see server logs for details");
            id = null;
        }
        CookieSessionImpl s = new CookieSessionImpl(SESSION_TIMEOUT, id);
        if(id != null){ //only read session data if id is present!
            s = scEncrypter.readSessionData(s, cs);
        }
        cs.setCookie(SESSION_COOKIE_NAME, s.ID);  
        return s;
    }    
    
    
   public static CookieSessionHandler create(Application a) throws Exception{
      return new CookieSessionHandler(a);
    }
    
    
    /***
     * CAREFULL
     * Set the session timeout
     * @param l
     * @return 
     */
    @Override
    public SessionHandler setSessionTimeout(long l) {
        this.SESSION_TIMEOUT = l;
        scEncrypter.setTtl(l);
        return this;
    }

    /***
     * @todo NOT IMPLEMENTED YET
     * 
     * Default of whether a nagging log warning should be written if the session handler is accessed over HTTP, not HTTPS
     * @param bln
     * @return 
     */
    @Override
    public SessionHandler setNagHttps(boolean bln) {
        this.NAG_HTTPS = bln;
        logger.error("NAG_HTTPS NOT YET SUPPORTED, SRY!");
        return this;
    }

    @Override
    public SessionHandler setSessionCookieName(String string) {
        this.SESSION_COOKIE_NAME = string;
        return this;
    }    

    public String getSESSION_COOKIE_NAME() {
        return SESSION_COOKIE_NAME;
    }

    public long getSESSION_TIMEOUT() {
        return SESSION_TIMEOUT;
    }

    public boolean isNAG_HTTPS() {
        return NAG_HTTPS;
    }

    /***
     * @todo NOT IMPLEMENTED YET
     * 
     * @param bln
     * @return 
     */
    @Override
    public SessionHandler setCookieSecureFlag(boolean bln) {
        this.COOKIE_SECURE_FLAG = bln;
        logger.error("COOKIE_SECURE_FLAG NOT YET SUPPORTED, SRY!");
        return this;
    }

    /***
     * @todo NOT IMPLEMENTED YET
     * 
     * @param bln
     * @return 
     */
    @Override
    public SessionHandler setCookieHttpOnlyFlag(boolean bln) {
        this.COOKIE_HTTP_ONLY_FLAG = bln;
        logger.error("COOKIE_HTTP_ONLY_FLAG NOT YET SUPPORTED, SRY!");
        return this;
    }

    /* @todo check api for 3.4
    @Override
    public SessionHandler setMinLength(int i) {
        throw new UnsupportedOperationException("Not supported yet.");
        //return this;
    }*/

    @Override
    public SessionHandler setMinLength(int i) {
        logger.info("ignoring unsupported method call SessionHandler.setMinLength()");
        return this;
    }

}
