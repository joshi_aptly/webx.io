/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.http.session;

import io.wx.core3.security.Encrypter;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Session;
import io.wx.core3.http.cookies.CookieStore;
import io.wx.core3.common.WXTools;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
public class CookieSessionEncrypter {
    private static final Logger logger = LogManager.getLogger(CookieSessionEncrypter.class);
    private static final String sessionCookieName = "wxsessiondata";
    private static final String sessionCookieSignature = "wxsign"; //session cookie signaturename    
    private static final String sessionCookieExpires = "wxexpires";
    private final Encrypter encryper;
    private long ttl;
    
    
    
    /***
     *
     * @param e
     * @param ttl time to life in miliseconds!!
     */
    public CookieSessionEncrypter(Encrypter e, long ttl){
        this.encryper = e;
        this.ttl = ttl;
    }
    
    
    /***
     * somehow the browser doenst send the cookie correctly encoded?!?!
     * @param str
     * @return 
     */
    public String ensureURLEncode(String str){
       if(str != null){
            str = str.replace("/", "%2F");
            str = str.replace("+", "%2B");
       }
       return str;
    }
    
    
    /***
     * populates and validates the given session from the cookiestore
     * @param s the session
     * @param cs the cookiestore
     * @return populated or new session
     */
    public CookieSessionImpl readSessionData(CookieSessionImpl s, CookieStore cs){
        StringBuilder signMe = new StringBuilder(s.ID);
        /** single cookie impl */
         try{
             if(cs.getValue(sessionCookieName)==null){
                throw new InvalidSessionCookieSignatureException(sessionCookieName+" not present");
             }
             logger.debug("reading cookie");
             //logger.debug("raw cookie "+cs.getValue(sessionCookieName));
             /* somehow the browser doest send the / encoded...pffff */
             String cookieSignature = ensureURLEncode(cs.getValue(sessionCookieSignature));
             String fixMyCookieValue = ensureURLEncode(cs.getValue(sessionCookieName));  
             logger.debug("cookie value "+fixMyCookieValue);
             logger.debug("cookie signature "+cookieSignature);
             String decrypt = encryper.decrypt( URLDecoder.decode(fixMyCookieValue, "UTF-8") );
             signMe.append(decrypt);               
             if(cs.getValue(sessionCookieExpires) != null){
                 Long getExpireTime =  Long.parseLong(cs.getValue(sessionCookieExpires));
                 if (new Date().getTime() > getExpireTime){
                     throw new ExpiredSessionCookieException("Cookie is expired at "+new Date(getExpireTime));
                 }
                 signMe.append(getExpireTime);
             }
             String sign = encryper.sign(signMe.toString());
             sign = URLEncoder.encode(sign, "UTF-8");     
             logger.debug("validate sign : "+signMe.toString());
             if(!sign.equals(cookieSignature)){
                throw new InvalidSessionCookieSignatureException("invalid client session signature");
                //throw new InvalidSessionCookieSignatureException("Clientvalue : "+cs.getValue(sessionCookieSignature)+" != Servervalue: "+sign);
             }                 
             Map<String, Object> sessionMap = WXTools.fromJSON(decrypt);
             for(String sessionKey : sessionMap.keySet()){
                 try{
                    String rawValue = (String)sessionMap.get(sessionKey);                     
                    JsonObject wrapper = new JsonObject(rawValue);                   
                    Class clazz = Class.forName(wrapper.getString("__class"));
                    if(clazz == String.class){
                        s.data.put(sessionKey, wrapper.getString("data"));
                    }
                    else{
                        s.data.put(sessionKey,WXTools.mapper.readValue(wrapper.getString("data"), clazz));                        
                    }
                 }catch(Exception ex){
                     logger.error("cant read session value for "+sessionKey,ex);
                 }
             }
         }
         catch(Exception ex){
             if(ex instanceof ExpiredSessionCookieException){
                 logger.debug(ex);
             }
             else{
                logger.error("cant read session cookie ",ex);                
             }
             s = new CookieSessionImpl(s.timeout(),null);              
         }
         return s;      
    }
    
    
    public void saveSession(Session s, CookieStore cs){
        StringBuilder signMe = new StringBuilder(s.id());
        Map<String, String> toCookieVal = new HashMap<String, String>();
        for (String sKey : s.data().keySet()) {
            if(sKey == null){
                continue;
            }
            try {
                Object sObject = s.data().get(sKey);
                if(sObject==null){
                    continue;
                }
                String sObjectAsString = null;
                if (sObject instanceof String) { //test if required?
                    sObjectAsString = (String) sObject;
                } else {
                    sObjectAsString = WXTools.toJSON(sObject);
                }
                JsonObject wrapper = new JsonObject();
                wrapper.put("__class", sObject.getClass().getName());
                wrapper.put("data", sObjectAsString);
                sObjectAsString = wrapper.encode();
                /* single cookie impl*/
                toCookieVal.put(sKey, sObjectAsString);      
            } catch (Exception ex) {
                logger.error("cant save session cookie "+sKey, ex);
            }
        }
        try {
            /*single cookie value impl*/
            String myCookieVal = WXTools.toJSON(toCookieVal);
            signMe.append(myCookieVal);
            logger.debug("session to combined cookie value "+myCookieVal);
            String encryp = encryper.encrypt(myCookieVal);
            String encode =  URLEncoder.encode(encryp, "UTF-8");
            logger.debug("my cookie value "+encode);
            cs.setCookie(sessionCookieName, encode);
        } catch (Exception ex) {
            logger.error("cant save cookie session value ",ex);
        }
        if(ttl>0){
            Date now = new Date();
            Long getExpire = now.getTime() + ttl;
            cs.setCookie(sessionCookieExpires, getExpire.toString());
            signMe.append(getExpire.toString());
        }        
        logger.debug("signing cookie values "+signMe);
        try {
            cs.setCookie(sessionCookieSignature, URLEncoder.encode(encryper.sign(signMe.toString()), "UTF-8"));
        } catch (Exception ex) {
            logger.error("cant sign cookies");
        }
        /*i need to clear cookies i guess :| ..only when using one cookie per session val
        for(String cookieName : r.getCookieStore().getCookieNames()){
            if(cookieName.startsWith(sessionCookiePrefix)){
                if(!s.data.containsKey(cookieName.replace(sessionCookiePrefix, ""))){ 
                    r.getCookieStore().deleteCookie(cookieName);//session val isnt present in the session right now, go away
                }
             }
        }*/        
    }

    public long getTtl() {
        return ttl;
    }

    public void setTtl(long ttl) {
        this.ttl = ttl;
    }
 
    
    
    
    
    
}
