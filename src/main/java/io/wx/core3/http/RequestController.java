/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http;

import com.google.inject.Module;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import io.wx.core3.http.cookies.CookieStore;
import io.wx.core3.http.exceptions.HandleResponseException;
import java.util.Map;
import java.util.Set;

/**
 * http requestcontroller interface
 *
 * @author moritz
 */
public interface RequestController {

    /***
     * gets the dependencie injection modules for the request scope
     * @return 
     */
    public Set<Module> getModules();
    
    /***
     * adds a dependencie injection module for the request scope
     * @param m
     * @return 
     */
    public RequestController addModule(Module m);
    
    /***
    * returns the controller machtes for the given request (does not contain trait matches!)
    * if no match exists webx returns a 404
    * @return 
    */
   public int getMatches();
   
   /**
    * adds a controller match to the given request 
    * @return 
    */
   public int doMatch();   
    
    /***
     * sets the response body
     * @param body
     * @return a reference to this, so the API can be used fluently
     */
    public RequestController setResponseBody(StringBuilder body);

    /**
     * writes/flushes the response body to the response
     * @return a reference to this, so the API can be used fluently
  
    public RequestController writeResponseBody();   */
    
    
     /***
     *  handle response based on @param result. supports String.class, Template.class or JSON/XML Serialization
     * @param result     
     * @return  current instance     
     * @throws io.wx.core3.http.exceptions.HandleResponseException     
     */
    public RequestController doResponse(Object result) throws HandleResponseException;
    
    /***
     * saves the current session (cookie store only)
     * @return 
     */
    public RequestController saveSession();
    
    /**
     * Fail the context with the specified status code.
     *  This will cause the router to route the context to any matching failure handlers for the request. If no failure handlers match a default failure response will be sent.
     * @param statusCode
     */
    void fail(int statusCode);

    /**
     * Fail the context with the specified throwable.
     *  This will cause the router to route the context to any matching failure handlers for the request. If no failure handlers match a default failure response with status code 500 will be sent.
     * @param throwable
     */
    void fail(Throwable throwable);

    /**
     * If the route specifies produces matches, e.g. produces `text/html` and `text/plain`,
     * and the `accept` header matches one or more of these then this returns the most acceptable match.
     * @return
     */
    String getAcceptableContentType();

    /*
     * Get the entire HTTP request body as a JsonObject.
     */
    JsonObject getBodyAsJson();

    CookieStore getCookieStore();

    /**
     * @return the current route this context is being routed through.
     */
    Route getCurrentRoute();

    /***
     * @return all the context data as a map
     */
    Map<String, Object> getData();

    /**
     * a set of fileuploads (if any) for the request
     *
     * @return
     */
    Set<FileUpload> getFileUploads();

    String getNormalisedPath();

    /**
     * the HTTP request object
     * @return
     */
    HttpServerRequest getRequest();

    /**
     * Get the entire HTTP request body as a Buffer.
     *
     * @return
     */
    Buffer getRequestBody();

    /***
     * gets the entire HTTP request body as a string, assuming UTF-8 encoding.
     *
     * @return the body
     */
    String getRequestBodyAsString();

    /***
     * Get the entire HTTP request body as a string, assuming the specified encoding.
     *
     * @param encoding the encoding, e.g. "UTF-16"
     * @return  the body
     */
    String getRequestBodyAsString(String encoding);

    /**
     * the HTTP response object
     *
     * @return
     */
    HttpServerResponse getResponse();

    /**
     * returns the responsebody for this request, this enables you to write the response when its ready
     * @return
     */
    StringBuilder getResponseBody();

    /***
     * returns the underlying request context, ONLY use this if you sure about what you doing
     * normaly all imporant functions can be access thru the HttpController itself
     * @return
     */
    RoutingContext getRoutingContext();

    Session getSession();

    /**
     * is the request in debug mode ?
     * @return 
     */
    Boolean isDebug();

    /**
     * Tell the router to route this context to the next matching route (if any).
     * This method, if called, does not need to be called during the execution of the handler,
     * it can be called some arbitrary time later, if required.
     */
    void next();

    /**
     * sets the debug mode for this request
     * @param b
     * @return
     */
    RequestControllerImpl setDebug(Boolean b);
    
}
