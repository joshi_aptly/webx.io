/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http.exceptions;

/**
 *
 * @author moritz
 */
@StatusCode(value = 500)
public class InvocationException extends Exception {

    /**
     * Creates a new instance of <code>InvocationException</code> without detail
     * message.
     */
    public InvocationException() {
    }

    /**
     * Constructs an instance of <code>InvocationException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public InvocationException(String msg) {
        super(msg);
    }
    
    public InvocationException(String msg, Throwable ex) {
        super(msg, ex);
    }


}
