package io.wx.core3.http.exceptions;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * http resource annotation
 * 
 * @author moritz
 */
@Retention( RetentionPolicy.RUNTIME )
public @interface StatusCode{
    int value() default 500; 
}
