/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.http.exceptions;

/**
 *
 * @author moritz
 */
@StatusCode(value = 500)
public class HandleResponseException extends Exception {

    /**
     * Creates a new instance of <code>HandleResponseException</code> without
     * detail message.
     */
    public HandleResponseException() {
    }

    /**
     * Constructs an instance of <code>HandleResponseException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public HandleResponseException(String msg) {
        super(msg);
    }
    
    public HandleResponseException(String msg, Throwable cause) {
        super(msg, cause);
    }    
    
}
