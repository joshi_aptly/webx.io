/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.common;

import io.vertx.core.json.JsonObject;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
public class ApplicationDetails {
    private static final Logger logger = LogManager.getLogger( ApplicationDetails.class );
    private Properties properties;
    private String propertiesFile = "cfg/build.properties";

    public ApplicationDetails(){
        
    }
    
    /****
     * 
     * @param propertiesFile fully qualified filename for your properties file 
     */
    public ApplicationDetails(String propertiesFile){
        this.propertiesFile = propertiesFile;
    }
    
    
    /***
     * returns all application properties matching the given propertyNames
     * 
     * will return an empty json object if the properties file can not be read or no match is found
     * 
     * @param propertyNames properties to read
     * @return 
     */    
    public JsonObject getProperties(Set<String> propertyNames){
        //try to load propertie file
        if(properties == null){
            try {         
                properties = new Properties();
                properties.load(  Files.newInputStream(Paths.get(propertiesFile)) );
            } catch (Exception ex) {
                logger.warn("can not load application properties :( ", ex);
            }
        }            
        JsonObject result = new JsonObject();
        if(properties != null){
            //if no propertyNames are defined, take all
            if(propertyNames == null){
                propertyNames = properties.stringPropertyNames();
            }
            for(String entry : propertyNames){ // Arrays.asList("release", "product", "version")
                result.put(entry, properties.getProperty(entry));
            }     
        }
        return result;    
    }
    
    /***
     * returns all application properties.
     * will return an empty json object if the properties file can not be read
     * 
     * @return 
     */
    public JsonObject getProperties(){
        return getProperties(new HashSet<>());
    }
    
    
    public JsonObject getRAM(){
        JsonObject result = new JsonObject();
        Runtime rt = Runtime.getRuntime();
        if(rt != null){
            result.put("total",  ( rt.totalMemory() / (1024*1024)) );
            result.put("free",  (rt.freeMemory() / (1024*1024)) );
            result.put("max", (rt.maxMemory() / (1024*1024)) );
        }
        return result;
    }
    
    
}
