/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.common;

import com.fasterxml.jackson.databind.util.StdConverter;
import io.vertx.core.json.JsonObject;
import java.util.Map;

/**
 *
 * @author joshi
 */
public class MapConverter extends StdConverter<Map<String, Object>, JsonObject>{
    @Override
    public JsonObject convert(Map<String, Object> value) {
        return new JsonObject(value);
    }
}
