package io.wx.core3.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.xml.bind.marshaller.CharacterEscapeHandler;
import com.sun.xml.bind.marshaller.DataWriter;

import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.wx.core3.http.Optional;
import io.wx.core3.http.Core;
import io.wx.core3.http.exceptions.ValidationException;
import io.wx.core3.xml.XMLResponse;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

/**
 * static tool functions
 * 
 * @author moritz.thielcke
 */
public class WXTools {
    private static Logger logger = LogManager.getLogger(WXTools.class);
    public static ObjectMapper mapper = new ObjectMapper();     ///json object mapper (@todo the static use is a bit ugly here)  
   
    
    /***
     * returns a handler to serve files in a given directory, @todo make hackx-save ?
     * @param onDirectory the directory to serve
     * @return the handler
     */
    public static  Handler<RoutingContext> serveFiles(String onDirectory){
        final String directory = onDirectory;
        return new Handler<RoutingContext>() {
            @Override
            public void handle(RoutingContext ctx) {
                File f;
                if(directory!=null){
                    f = new File(directory+ctx.request().path());
                }else{                  
                    f = new File("."+ctx.request().path());
                }
                if(f.exists()){
                    ctx.request().response().sendFile(f.getAbsolutePath());
                }
                else{
                    logger.debug("cant find requested file "+ctx.request().path());
                    send404Page(ctx.request());
                }
            }
        };
        
    }   
    
     
   /****
    * since sendfile always returns a http ok, we need to send the 404 error page manually
    * @todo please use async file reading , nub! :)
    * 
    * @param req 
    */
   public static void send404Page(HttpServerRequest req){
        req.response().setStatusCode(404);
        req.response().putHeader("Content-Type", "text/html; charset=utf-8");
        File f = new File("./public/404.html");
        StringBuilder stringB = new StringBuilder();        
        if(f.exists()){
            //req.response.sendFile(f.getAbsolutePath());
            FileReader r = null;
            try {
                r = new FileReader(f.getAbsolutePath());
                int c;
                while (-1 != (c = r.read())) {
                    stringB.append((char)c);
                }
                r.close();
            } catch (IOException ex) {
                logger.error("cant read 404.html",ex);
            }
            finally{
                if(r!=null){
                    //yeah ...
                    try { r.close(); } catch (IOException ex) {}
                }
            }
        }
        else{
            logger.error("no 404.html found but requested");
        }
        req.response().end(stringB.toString());       
   }  
    
   

   /**
    * @todo check for hackx
    * 
    * @param c
    * @return 
    */
    public static Handler<RoutingContext> createPublicDirectory(Core c) {
        return serveFiles(null);
    }
   
       
    
    /***
     * initializes jaxb
     *  only important for xml serialization (XMLRootElement's)
     * @param r
     * @return
     * @throws JAXBException 
     */    
    public static JAXBContext initJAXB(Reflections r) throws JAXBException{
        Set<Class<?>> annotated = r.getTypesAnnotatedWith(XmlRootElement.class);
        Class[] wtf = new Class[annotated.size()+1]; //cannot cast object to class ?!
        wtf[0] = io.wx.core3.xml.XMLResponse.class;
        int i=1;
        for (Class c : r.getTypesAnnotatedWith(XmlRootElement.class)){
            wtf[i] = c;i++;
        }   
        return JAXBContext.newInstance(wtf,null);
    }
   
    
    /***
     * serializes the given object to a json string
     * 
     * @param o the object
     * @return the json string
     */
    public static String toJSON(Object o) {
        try{
            return mapper.writeValueAsString(o);     
        }catch(IOException ex){
            logger.fatal("cant create JSON from Object ",ex);
            return null;
        }
    }
    
    
    /***
     * creates a Map from the given json
     * @param s
     * @return
     * @throws IOException 
     */
    public static Map<String , Object> fromJSON(String s) throws IOException{
        try{
            return mapper.readValue(s, Map.class); 
        }catch(IOException ex){
            logger.fatal("cant create MAP from JSON ",ex);
           return null;
        }
    }

       
    /***
     * object to xml-String serialization
     * @param o
     * @param jaxb
     * @return
     * @throws JAXBException 
     */
    public static String toXML(Object o, JAXBContext jaxb) throws JAXBException{
        JAXBContext ctx;
        //is the object is mapped by the global schema?
        if(o.getClass().isAnnotationPresent(XmlRootElement.class)){    
           ctx = jaxb;
        }
        else{
            //create schema for this object:
            //@todo cache?
            ctx = JAXBContext.newInstance(o.getClass(), XMLResponse.class);
            XMLResponse wrapper = new XMLResponse();
            wrapper.setObject( o );
            o = wrapper;
        }    
        StringWriter sw = new StringWriter();       
        try {
            Marshaller m = ctx.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            
             // The below code will take care of avoiding the conversion of < to &lt; and > to &gt; etc
            PrintWriter printWriter = new PrintWriter(sw);
            DataWriter dataWriter = new DataWriter(printWriter, "UTF-8", new CharacterEscapeHandler() {
                @Override
                public void escape(char[] ch, int start, int length,
                        boolean isAttVal, Writer writer)
                        throws IOException {
                    writer.write(ch, start, length);
                }
            });
            m.marshal(o, dataWriter);
            
        } catch (JAXBException ex) {
           logger.fatal("cant create marshaller for "+o,ex);
        }
        return sw.toString();
    }    
    


    /***
     * deprecated , see io.wx.core3.http.handler.DefaultFailureHandler
     * 
     * sends a servererror to the client
     * @param r response
     * @param body response body
     * @param msg error msg (can be null)
     * @param debug (show stacktrace?)
     * @param ex exception (can be null)
     */
    @Deprecated
    public static void sendServerErrorAsJSON(HttpServerResponse r, StringBuilder body, String msg, Boolean debug , Exception ex, int statusCode){
        if(msg == null){
            msg = "internal server error";
        }
        JsonObject error = new JsonObject();
        error.put("error", msg);
        if(debug && ex != null){
            JsonObject myException = new JsonObject();
            String exceptionMsg = ex.getMessage() != null ? ex.getMessage() : msg;
            myException.put("message", exceptionMsg);
            myException.put("type", ex.getClass().getSimpleName());
            error.put("exception", myException); 
            if(ex.getCause()!=null){
                JsonObject cause = new JsonObject();
                cause.put("message", ex.getCause().getMessage());
                cause.put("type", ex.getCause().toString());
                JsonArray stcktrc = new JsonArray();
                for(StackTraceElement trace : ex.getCause().getStackTrace() ){
                    stcktrc.add(trace.toString());
                }        
                cause.put("stacktrace", stcktrc);
                error.put("cause", cause);
            }    
            JsonArray stcktrc = new JsonArray();
            for(StackTraceElement trace : ex.getStackTrace()){
                stcktrc.add(trace.toString());
            }
            myException.put("stacktrace", stcktrc);
        }
        if(ex != null && ex instanceof ValidationException ){
            JsonArray violations = new JsonArray();
            for( ConstraintViolation v : ((ValidationException)ex).getViolations() ){
                JsonObject vDetails = new JsonObject();
                vDetails.put("field", v.getPropertyPath().toString());
                vDetails.put("error", v.getMessage());
                violations.add(vDetails);
            }
            error.put("validationErrors", violations);
        }
        r.setStatusCode(statusCode); 
        r.putHeader("Content-Type", "application/json; charset=utf-8");
        r.end(error.encodePrettily());  
    }

   

    /***
     * * deprecated , see io.wx.core3.http.handler.DefaultFailureHandler
     * 
     * sends a http 500 to the client
     * @param r
     * @param body
     * @param msg optional msg (
     * @param debug  show stacktrace?
     * @param ex  optional exception
     */
    @Deprecated
    public static void  sendServerError(HttpServerResponse r, StringBuilder body, String msg, Boolean debug , Exception ex){
        sendServerErrorAsJSON(r,body, msg, debug, ex, 500);
    }
    
 
    
    
    /***
     * * deprecated , see io.wx.core3.http.handler.DefaultFailureHandler
     * 
     * send error to  client
     * @param r
     * @param body
     * @param msg optional msg (can be null)
     * @param debug  show stacktrace?
     * @param ex  optional exception
     * @param statusCode
     */
    @Deprecated
    public static void  sendServerError(HttpServerResponse r, StringBuilder body, String msg, Boolean debug , Exception ex, int statusCode){
        sendServerErrorAsJSON(r,body, msg, debug, ex, statusCode);
    }

    
    
    /***
     *  sends a http 303
     * @param r  the server response object
     * @param url the url to redirect to
     */
    public static void sendRedirect(HttpServerResponse r, String url){
        //r.headers().put("Location", url);
        r.headers().set("Location", url);
        r.setStatusCode(303);
    }
    
    
    /***
     * reads out the http Authorization: Bearer -token
     * @param request
     * @return the token,  null if not set
     */
    public static String getBearerToken(HttpServerRequest request){
        return getAuthorizationHeader(request, "bearer");
    }
    
    /***
     * reads the basic http authorization header
     * @param request
     * @return 
     */
    public static String getBasicAuth(HttpServerRequest request){
        return getAuthorizationHeader(request, "basic");
    }
    
    
    /***
     * reads the  http authorization header
     * 
     * @param request
     * @param type authorization type: bearer, basic, 
     * @return null or the given header value
     */
    public static String getAuthorizationHeader(HttpServerRequest request , String type){
        String result = null;
        String httpAuth = request.headers().get("Authorization");
        if(httpAuth != null && type != null){
            type = type.toLowerCase();
            String[] getToken = request.headers().get("Authorization").split(" ");
            if(getToken.length > 1 && getToken[0].toLowerCase().equals(type)){
                result = getToken[1];
            }
        }
        return result;       
    }
    
    /***
     * checks if the given annotation array contains the @Optional annotation
     * @param annotations
     * @return 
     */
    public static Boolean containsOptional(Annotation[] annotations){
        for(Annotation a : annotations){
            if(a instanceof Optional){
                return true;
            }
        }
        return false;
    }

    /***
     * loads credentials from the given filename
     *  returns null if the file does not exists
     * @param fileName
     * @return 
     */
    public static JsonObject loadCredentials(String fileName){
        if(new File(fileName).exists()){
            try{
                String str = new String( Files.readAllBytes(Paths.get(fileName)) );
                return new JsonObject(str);
            }catch(Exception ex){
                 logger.error("cant load credentials from "+fileName,ex);
            }            
        }
        return null;
    }
    
}
