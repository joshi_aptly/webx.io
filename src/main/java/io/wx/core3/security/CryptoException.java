/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.security;

/**
 *
 * @author moritz
 */
public class CryptoException extends Exception {

    /**
     * Creates a new instance of <code>CryptoException</code> without detail
     * message.
     */
    public CryptoException() {
    }

    /**
     * Constructs an instance of <code>CryptoException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public CryptoException(String msg) {
        super(msg);
    }
    
    
    public CryptoException(String msg, Throwable ex){
        super(msg,ex);
    }
}
