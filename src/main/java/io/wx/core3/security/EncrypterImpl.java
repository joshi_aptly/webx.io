/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.security;

import java.lang.reflect.Field;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



/**
 * our HmacSHA1 encrypter imlementation
 *
 * @author moritz
 */
public class EncrypterImpl implements Encrypter {   
    private static Logger logger = LogManager.getLogger(EncrypterImpl.class);
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
    private Key key;   
    private Key signingKey;
    private String ALGO;
    
    
    static{
        try { 
            Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
            field.setAccessible(true);
            field.set(null, java.lang.Boolean.FALSE); 
        } catch (Exception ex) {
            logger.debug("cant set javax.crypto.JceSecurity.isRestricted");
        }        
    }

    
    public EncrypterImpl(String mySecret, String mySalt) throws NoSuchAlgorithmException, InvalidKeySpecException{
        this(mySecret, mySalt, "AES");
    }
    
    public EncrypterImpl(String mySecret, String salt, String algo) throws NoSuchAlgorithmException, InvalidKeySpecException{
        this.ALGO = algo;
        this.key = generateKey(mySecret, salt);
        this.signingKey = generateSigningKey(mySecret);
    }

    
    /***
     * 
     * @param ENCRYPT_MODE see Cipher.ENCRYPT_MODE
     * @return
     * @throws CryptoException 
     */
    public Cipher getCipher(int ENCRYPT_MODE) throws CryptoException{
        Cipher c;
        try {
            c = Cipher.getInstance(ALGO);
        } 
        catch (NoSuchAlgorithmException | NoSuchPaddingException ex) {
            throw new CryptoException("cant initialize cipher, please report as bug ",ex);
        }
        try {
            c.init(ENCRYPT_MODE, key); //@todo does it makes sense to cache this one,  probly not thread save :|
        } 
        catch (InvalidKeyException ex) {
           throw new CryptoException("Cipher throws InvalidKeyException, please make sure to install the right JCE unlimited strength jurisdiction policy files."
                                    + " for more info read  http://stackoverflow.com/questions/6481627/java-security-illegal-key-size-or-default-parameters"
                                    , ex);
        }
        return c;
    }
    
    
    @Override
    public String encrypt(String Data) throws CryptoException, IllegalBlockSizeException, BadPaddingException{
        Cipher c = getCipher(Cipher.ENCRYPT_MODE);
        byte[] encVal = c.doFinal(Data.getBytes());
        String encryptedValue =   Base64.getEncoder().encodeToString(encVal) ;
        return encryptedValue;
    }

    @Override
    public String decrypt(String encryptedData) throws CryptoException, BadPaddingException, IllegalBlockSizeException{
        Cipher c = getCipher(Cipher.DECRYPT_MODE);
        //Cipher c = Cipher.getInstance(ALGO);
        //c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = Base64.getDecoder().decode(encryptedData);
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }

    private Key generateKey(String pwd, String salt) throws NoSuchAlgorithmException, InvalidKeySpecException{
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        //new PBEKeySpec
        Key hashedPwd = factory.generateSecret( new PBEKeySpec( pwd.toCharArray(), salt.getBytes() , 50000, 256) ); //256bit key
        return new SecretKeySpec(hashedPwd.getEncoded(), ALGO);
    }

    private Key generateSigningKey(String pwd){
        return new SecretKeySpec(pwd.getBytes(), HMAC_SHA1_ALGORITHM);  
    }

    @Override
    public String sign(String data) throws SignatureException {
        String result=null;
        try {
            // get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signingKey);
            // compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(data.getBytes());
            // base64-encode the hmac
            result = Base64.getEncoder().encodeToString(rawHmac);
        }
        catch (Exception e) {
            throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
        }      
        return result;
    }
    
}
