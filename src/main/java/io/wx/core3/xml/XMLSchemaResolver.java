package io.wx.core3.xml;

import java.io.File;
import java.io.IOException;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * xml schema resolver for xml
 * 
 * @author moritz
 */
public class XMLSchemaResolver extends SchemaOutputResolver {
    File directory;
    
    public XMLSchemaResolver(File directory){
        super();
        this.directory = directory;
    }

    
    @Override
    public Result createOutput(String nameSpace, String fileName) throws IOException {
        return new StreamResult(new File(directory, fileName));
    }
   
}
