package io.wx.core3.xml;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 *  wrapper object for xml responses
 * @author moritz
 */
@XmlRootElement
public class XMLResponse<T> {
    
    private  T object;

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }

    
    
    
}
